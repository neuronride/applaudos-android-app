package com.android.applaudos.aldoschallenge.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * String utilities class
 *
 * @author Aldo on 1/22/2016.
 */
public class StringUtil {
    /**
     * Wherever a string is null or empty
     *
     * @param text The string variable to analyze
     * @return True for null or empty
     */
    public static Boolean stringIsNullOrEmpty(String text) {
        return !(text != null && !text.isEmpty());
    }

    /**
     * Validates an email against regex method
     *
     * @param email The email address
     * @return true when email format is valid
     */
    public static Boolean isValidEmailString(String email) {
        if (StringUtil.stringIsNullOrEmpty(email)) {
            return false;
        }

        Pattern emailAddressPattern = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+"
        );

        return emailAddressPattern.matcher(email).matches();
    }

    /**
     * produces a time stamp
     *
     * @param locale The time zone and language locale
     * @return new date time time stamp
     */
    public static String getDateTimeStamp(Locale locale) {
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("MM/dd/yyyy'T'hh:mm:ss", locale);

        String format = simpleDateFormat.format(new Date());

        return format;
    }

    /**
     * Fix format to show always two numbers
     *
     * @param time The time  to be set to the correct hours
     * @return The new format number
     */
    public static String timeFixTwoDigit(int time) {
        String result = time + "";

        if (result.length() == 1) {
            result = "0" + result;
        }

        return result;
    }

    /**
     * Create a time stamp for timers
     *
     * @param secondDuration The seconds durations
     * @return The new time stamp
     */
    public static String timerDurationSetter(int secondDuration) {
        int seconds = secondDuration % 60,
                min = (int) Math.floor(secondDuration / 60),
                hour = (int) Math.floor(min / 60);

        String timeStamp;

        if (hour >= 1) {
            timeStamp = timeFixTwoDigit(hour) + ":" +
                    timeFixTwoDigit(min) + ":" +
                    timeFixTwoDigit(seconds);
        } else {
            timeStamp = timeFixTwoDigit(min) + ":" +
                    timeFixTwoDigit(seconds);
        }

        return timeStamp;
    }

    /**
     * Capitalizes Each Word first character
     *
     * @param text The Text
     * @return The new word with each word capitalized
     */
    public static String capitalizeByWord(String text) {
        StringBuilder result = new StringBuilder();
        String[] strArr = text.split(" ");
        for (String str : strArr) {
            char[] stringArray = str.trim().toCharArray();
            stringArray[0] = Character.toUpperCase(stringArray[0]);
            str = new String(stringArray);

            result.append(str).append(" ");
        }
        return result.toString().trim();
    }

    /**
     * Determine if an string is a valid full name
     *
     * @return Wherever is a valid full name
     */
    public static boolean isValidFullName(int minLength, String name) {
        if (stringIsNullOrEmpty(name) ||
                name.length() < minLength) {

            return false;
        }

        String regex = "^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(name);

        return matcher.matches();
    }

    /**
     * Validates if is a numeric value
     *
     * @param text The text number
     * @return if is valid number
     */
    public static boolean isNumeric(String text) {
        if (stringIsNullOrEmpty(text)) {
            return false;
        }

        for (char c : text.toCharArray()) {

            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Validates if the text is good for a phone number
     *
     * @param numberSize The number of digits to be valid
     * @param text       The text number
     * @return if is a valid phone number
     */
    public static boolean isValidLocalPhone(int numberSize, String text) {
        text = text.trim();

        if (stringIsNullOrEmpty(text) ||
                text.length() == numberSize) {
            return isNumeric(text);
        }

        return false;
    }

    /**
     * Removes extra spaces
     *
     * @param text The Text
     * @return The new word without extra spaces
     */
    public static String trimExtraSpaces(String text) {
        StringBuilder result = new StringBuilder();

        for (int nIndex = 0; nIndex < text.length(); nIndex++) {
            if (text.charAt(nIndex) != ' ') {
                result.append(text.charAt(nIndex));
            } else if (result.length() > 0 && result.charAt(result.length() - 1) != ' ') {
                result.append(text.charAt(nIndex));
            }
        }

        return result.toString().trim();
    }

    /**
     * Converts any input stream into a String
     *
     * @param inputStream The input stream to transform
     * @return The String from the input Stream
     */
    public static String convertStreamToString(InputStream inputStream) {
        try {
            Reader reader = new BufferedReader(new InputStreamReader(inputStream,
                    "UTF-8"));

            Writer writer = new StringWriter();

            char[] buffer = new char[2048];

            int n;

            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }

            inputStream.close();

            return writer.toString();
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Converts a normal string to an hex string
     *
     * @param normalString The string to convert to hex
     * @return The string as hex string
     */
    public static String toHexString(String normalString) {

        char[] bytes = normalString.toCharArray();

        StringBuilder str = new StringBuilder();

        for (int i = 0; i < bytes.length; i++) {
            str.append(String.format("%02x", (int) bytes[i]));
        }

        return str.toString();
    }

    /**
     * Converts an hex string to an original value
     *
     * @param hex The hex string to convert to normal string
     * @return The normal string
     */
    public static String fromHexString(String hex) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            str.append((char) Integer.parseInt(hex.substring(i, i + 2), 16));
        }
        return str.toString();
    }
}