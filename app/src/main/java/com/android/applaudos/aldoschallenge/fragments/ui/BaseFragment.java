package com.android.applaudos.aldoschallenge.fragments.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.android.applaudos.aldoschallenge.activities.BaseActivity;

/**
 * Base Functionality for fragments
 *
 * @author Aldo on 1/22/2016.
 */
public class BaseFragment extends Fragment {
    /**
     * Base Activity
     */
    private BaseActivity mBaseActivity;

    /**
     * On Fragment Create
     *
     * @param savedInstanceState The current saved instances
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBaseActivity = (BaseActivity) getActivity();
    }

    /**
     * On Resume
     */
    @Override
    public void onResume() {
        super.onResume();

        if (mBaseActivity == null) {
            mBaseActivity = (BaseActivity) getActivity();
        }
    }

    /**
     * The current base activity
     */
    public BaseActivity getBaseActivity() {
        if (mBaseActivity == null) {
            mBaseActivity = (BaseActivity) getActivity();
        }

        return mBaseActivity;
    }

    /**
     * The fragment tag name
     *
     * @return The current fragment class name from instance
     */
    public String getTagName() {
        return this.getClass().getName();
    }
}