package com.android.applaudos.aldoschallenge.activities;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.fragments.ui.BaseFragment;
import com.android.applaudos.aldoschallenge.fragments.ui.dialog.BaseDialogFragment;
import com.android.applaudos.aldoschallenge.utils.AnimationHelpers;
import com.android.applaudos.aldoschallenge.utils.StringUtil;
import com.android.applaudos.aldoschallenge.widgets.LoadingFullScreen;

/**
 * Base Activity class helper to set common operations to all activities
 *
 * @author Aldo Infanzon on 4/15/2016.
 */
public class BaseActivity extends AppCompatActivity {

    /**
     * Fade Animation tag
     */
    public static final String FADE_ANIMATION = "fade";

    /**
     * For no animation on fragment transition
     */
    public static final String NO_ANIMATION = "none";

    /**
     * The loading screen
     */
    private LoadingFullScreen mLoadingScreen;

    /**
     * The animation helper instance
     */
    private AnimationHelpers mAnimationHelpers;

    /**
     * if The device is tablet
     */
    private boolean mIsTablet;

    /**
     * If the device is tablet and is the 6 to 8 inch tablet
     */
    private boolean mIsSw600;

    /**
     * If the device is tablet and is the 9 inch tablet
     */
    private boolean mIsSw720;

    /**
     * If the device is tablet and is the 10 or bigger inch tablet
     */
    private boolean mIsSw840;

    /**
     * The current fragment
     */
    private BaseFragment mCurrentFragment;

    /**
     * On General on create method
     * [Note] We removed Override Annotation to make sure we hit this method first
     *
     * @param savedInstanceState The saved instances
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mIsTablet = getResources().getBoolean(R.bool.isTablet);
        mIsSw600 = getResources().getBoolean(R.bool.isSw600);
        mIsSw720 = getResources().getBoolean(R.bool.isSw720);
        mIsSw840 = getResources().getBoolean(R.bool.isSw840);
    }

    /**
     * Helper to apply fragment transition
     *
     * @param fragment      The fragment to switch
     * @param frameLayoutId The Frame layout control id
     * @param animationName the animation name. Accepts null or empty
     */
    public void switchFragment(BaseFragment fragment, int frameLayoutId,
                               String animationName) {
        try {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = manager.beginTransaction();

            if (!StringUtil.stringIsNullOrEmpty(animationName)) {
                animationName = animationName.toLowerCase();

                switch (animationName) {
                    case "fade":
                        fragmentTransaction.setCustomAnimations(
                                R.anim.fade_in, R.anim.fade_out,
                                R.anim.fade_in, R.anim.fade_out);
                        break;
                    case NO_ANIMATION: // For no animation
                    default:
                        break;
                }
            }

            fragmentTransaction.replace(
                    frameLayoutId,
                    fragment,
                    fragment.getTagName());

            fragmentTransaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Helper to apply fragment transition
     *
     * @param fragment      The fragment to switch
     *                      (Id of frame layout must be R.id.fragment_container)
     * @param animationName the animation name. Accepts null or empty
     */
    public void switchFragment(BaseFragment fragment, String animationName) {

        try {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = manager.beginTransaction();

            if (!StringUtil.stringIsNullOrEmpty(animationName)) {
                animationName = animationName.toLowerCase();

                switch (animationName) {
                    case "fade":
                        fragmentTransaction.setCustomAnimations(
                                R.anim.fade_in, R.anim.fade_out,
                                R.anim.fade_in, R.anim.fade_out);
                        break;
                    case NO_ANIMATION: // For no animation
                    default:
                        break;
                }
            }

            fragmentTransaction.replace(
                    R.id.fragment_container,
                    fragment,
                    fragment.getTagName());

            fragmentTransaction.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Shows Fragment Dialog
     *
     * @param fragment the fragment to be shown
     */
    public void showCustomFragmentDialog(final BaseDialogFragment fragment) {
        try {
            this.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    FragmentManager fragmentManager = getSupportFragmentManager();

                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    android.support.v4.app.Fragment prev = fragmentManager
                            .findFragmentByTag(fragment.getTagName());

                    if (prev != null) {
                        DialogFragment df = (DialogFragment) prev;

                        df.dismiss();

                        fragmentTransaction.remove(prev);
                    }

                    fragmentTransaction.addToBackStack(null);

                    fragment.show(fragmentTransaction, fragment.getTagName());
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * if The device is tablet
     */
    public boolean isTablet() {
        return mIsTablet;
    }

    /**
     * If the device is tablet and is the 6 to 8 inch tablet
     */
    public boolean isSw600() {
        return mIsSw600;
    }

    /**
     * If the device is tablet and is the 9 inch tablet
     */
    public boolean isSw720() {
        return mIsSw720;
    }

    /**
     * If the device is tablet and is the 10 or bigger inch tablet
     */
    public boolean isSw840() {
        return mIsSw840;
    }

    /**
     * The current Fragment
     */
    public BaseFragment getCurrentFragment() {
        return mCurrentFragment;
    }

    /**
     * Sets the current Fragment
     */
    public void setCurrentFragment(BaseFragment fragment) {
        mCurrentFragment = fragment;
    }

    /**
     * The Activity tag name
     *
     * @return The current activity class name from instance
     */
    public String getTagName() {
        return this.getClass().getName();
    }

    /**
     * Loading helper screen. Make sure your activity has view_loading_fullscreen
     *
     * @return The Loading Screen instance
     */
    public LoadingFullScreen getLoadingScreen() {
        if (mLoadingScreen == null) {
            mLoadingScreen = new LoadingFullScreen(this);
        }

        return mLoadingScreen;
    }

    /**
     * The animation helper
     *
     * @return The animation helper
     */
    public AnimationHelpers getAnimationHelpers() {
        if (mAnimationHelpers == null) {
            mAnimationHelpers = new AnimationHelpers(this);
        }

        return mAnimationHelpers;
    }
}
