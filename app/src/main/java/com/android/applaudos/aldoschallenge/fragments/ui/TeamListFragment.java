package com.android.applaudos.aldoschallenge.fragments.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.activities.MainActivity;
import com.android.applaudos.aldoschallenge.adapters.TeamListAdapter;
import com.android.applaudos.aldoschallenge.utils.NetworkStateUtil;
import com.android.applaudos.challenge.framework.model.database.Team;

import java.util.List;

import okhttp3.Call;

/**
 * History Requests Screen Fragment
 */
public class TeamListFragment extends BaseFragment {
    /**
     * The current web call
     */
    private Call mCall;

    /**
     * Item recycler view adapter
     */
    private RecyclerView mRecyclerView;

    /**
     * The Team Recycler view list adapter
     */
    private TeamListAdapter mRecyclerAdapter;

    /**
     * The progress bar
     */
    private ProgressBar mProgressBar;

    /**
     * The no items text view
     */
    private TextView mNoItemsText;

    /**
     * The Refresh layout
     */
    private SwipeRefreshLayout mListRefresh;

    /**
     * The main root view
     */
    private View mRootView;

    /**
     * Class Constructor
     */
    public TeamListFragment() {
        // Required empty public constructor
    }

    /**
     * Creates a new instance
     *
     * @return The Fragment instance
     */
    public static TeamListFragment newInstance() {
        TeamListFragment fragment = new TeamListFragment();

        Bundle args = new Bundle();

        fragment.setArguments(args);

        return fragment;
    }

    /**
     * On Fragment creation
     *
     * @param savedInstanceState The current saved instances
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * On Fragment create view
     *
     * @param inflater           The inflater
     * @param container          The container
     * @param savedInstanceState The saved instances
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_team_list, container, false);

        mProgressBar = (ProgressBar) mRootView.findViewById(R.id.progress_bar);
        mNoItemsText = (TextView) mRootView.findViewById(R.id.no_items_text_view);

        mListRefresh = (SwipeRefreshLayout) mRootView.findViewById(R.id.swipe_refresh_layout);
        mListRefresh.setOnRefreshListener(new OnRefreshListener());

        // Set Recycler view
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerAdapter = new TeamListAdapter(getBaseActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mRecyclerAdapter);
        mRecyclerView.getItemAnimator().setChangeDuration(0);

        return mRootView;
    }

    /**
     * On Resume
     */
    @Override
    public void onResume() {
        super.onResume();

        boolean isOnline = NetworkStateUtil.isConnected(getBaseActivity());

        MainActivity activity = (MainActivity) getBaseActivity();

        activity.setCurrentFragment(this);

        // If the call was previously done we cancel it and then we try it again
        if (mCall != null && !mCall.isCanceled()) {
            mCall.cancel();
        }

        // Before running this on each resume we check if it is the first run
        if (activity.isOnFirstRun()) {
            if (isOnline) {
                mCall = activity.getDataFragment()
                        .getTeams();
            } else {
                activity.getDataFragment().getDbTeamsAsync();
            }
        } else {
            activity.getDataFragment().getDbTeamsAsync();
        }

    }

    /**
     * On get elements error
     *
     * @param teamList The items in case they come from db
     */
    public void onUpdateTeamsError(List<Team> teamList) {
        onUpdateTeamsSuccess(teamList);
    }

    /**
     * On Get Team List Success
     *
     * @param teamList The order items
     */
    public void onUpdateTeamsSuccess(List<Team> teamList) {
        MainActivity activity = (MainActivity) getBaseActivity();

        mListRefresh.setRefreshing(false);

        if (activity.isOnFirstRun()) {

            if (teamList == null || teamList.size() == 0) {

                if (mProgressBar.getVisibility() == View.VISIBLE) {

                    getBaseActivity().getAnimationHelpers()
                            .fadeSwitchViews(
                                    mProgressBar,
                                    mNoItemsText,
                                    null);
                }
            } else {
                mRecyclerAdapter.updateItemList(teamList);

                if (mNoItemsText.getVisibility() == View.VISIBLE) {

                    getBaseActivity().getAnimationHelpers()
                            .fadeSwitchViews(
                                    mNoItemsText,
                                    mRecyclerView,
                                    null);

                    activity.setIsOnFirstRun(false);

                    return;
                }

                getBaseActivity().getAnimationHelpers()
                        .fadeSwitchViews(
                                mProgressBar,
                                mRecyclerView,
                                null);

                activity.setIsOnFirstRun(false);
            }
        } else {
            activity.setIsOnFirstRun(false);

            mRecyclerAdapter.updateItemList(teamList);

            getBaseActivity().getAnimationHelpers()
                    .fadeSwitchViews(
                            mProgressBar,
                            mRecyclerView,
                            null);
        }
    }

    /**
     * On Refresh listener helper
     */
    private class OnRefreshListener implements SwipeRefreshLayout.OnRefreshListener {
        /**
         * Wherever user activates on refresh
         */
        @Override
        public void onRefresh() {
            mListRefresh.setRefreshing(true);

            boolean isOnline = NetworkStateUtil.isConnected(getBaseActivity());

            MainActivity activity = (MainActivity) getBaseActivity();

            activity.getDataFragment().getTeams();

            if (isOnline) {
                mCall = activity.getDataFragment()
                        .getTeams();
            } else {
                activity.getDataFragment().getDbTeamsAsync();
            }
        }
    }
}
