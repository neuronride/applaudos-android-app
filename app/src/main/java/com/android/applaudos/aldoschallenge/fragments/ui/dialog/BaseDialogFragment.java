package com.android.applaudos.aldoschallenge.fragments.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Window;

/**
 * Base Dialog Fragment
 *
 * @author Aldo Infanzon on 5/03/2016.
 */
public class BaseDialogFragment extends DialogFragment {
    /**
     * The activity context
     */
    protected Context mContext;

    /**
     * Current Dialog instance
     */
    protected DialogFragment mDialog;

    /**
     * @return String the class name
     */
    public String getTagName() {
        return this.getClass().getName();
    }

    /**
     * The On Create Dialog Lifecycle method
     *
     * @param savedInstanceState The saved instances
     * @return The created dialog
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    /**
     * On Create event
     *
     * @param savedInstanceState The saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Saving current instance
        mDialog = this;

        if (getShowsDialog()) {
            setRetainInstance(true);
        }
    }
}