package com.android.applaudos.aldoschallenge;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.android.applaudos.challenge.framework.constants.ServiceConstants;
import com.crashlytics.android.Crashlytics;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;

/**
 * @author aldoinfanzon on 3/30/2017.
 */
public class AldosChallengeApplication extends Application {
    /**
     * The application instance
     */
    private static AldosChallengeApplication appInstance;

    /**
     * Ok Http Client Singleton instance
     */
    public static OkHttpClient okHttpClient;

    /**
     * On attach base context. Initializes correctly Multidex
     *
     * @param base The base context
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);
    }

    /**
     * On Application Create
     */
    @Override
    public void onCreate() {
        super.onCreate();

        appInstance = this;

        // We initialize ok http client singleton instance
        // Pass this instance to business class
        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(ServiceConstants.DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(ServiceConstants.DEFAULT_WRITE_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(ServiceConstants.DEFAULT_READ_TIMEOUT, TimeUnit.MILLISECONDS)
                .build();

        // We initialize crashlytics module
        Fabric.with(getAppInstance(), new Crashlytics());

        // We initialize our Database ORM
        FlowManager.init(new FlowConfig.Builder(this).build());
    }

    /**
     * Returns main app instance
     *
     * @return The main instance
     */
    public static AldosChallengeApplication getAppInstance() {
        return appInstance;
    }
}
