package com.android.applaudos.aldoschallenge.activities;

import android.os.Bundle;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.fragments.ui.SplashFragment;

/**
 * Initial Activity class
 *
 * @author aldoinfanzon on 03/30/2017
 */
public class LauncherActivity extends BaseActivity {

    /**
     * On Create
     *
     * @param savedInstanceState The saved instances
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launcher);

        SplashFragment fragment = SplashFragment.newInstance();

        switchFragment(fragment, BaseActivity.NO_ANIMATION);
    }
}
