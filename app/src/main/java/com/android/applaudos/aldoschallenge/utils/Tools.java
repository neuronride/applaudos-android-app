package com.android.applaudos.aldoschallenge.utils;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.applaudos.challenge.framework.constants.BaseConstants;
import com.android.applaudos.challenge.framework.constants.ServiceConstants;


/**
 * Tools for UI controls
 *
 * @author Aldo Infanzon
 */
public class Tools {

    /**
     * Darker a color by a factor
     *
     * @param hexColor The color in hex format
     * @param factor   The darken factor
     * @return The darker color
     */
    public static int colorDarker(String hexColor, float factor) {
        int color = Color.parseColor(hexColor),
                a = Color.alpha(color),
                r = Color.red(color),
                g = Color.green(color),
                b = Color.blue(color);

        return Color.argb(a,
                Math.max((int) (r * factor), 0),
                Math.max((int) (g * factor), 0),
                Math.max((int) (b * factor), 0));
    }

    /**
     * Simple toast helper
     *
     * @param context The current context
     * @param message The message
     */
    public static void showSimpleToast(Context context, String message) {
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

    /**
     * Goes to Store for rate and update
     *
     * @param context The Context
     */
    public static void goToStore(Context context) {

        String marketLink;

        if (!Tools.isKindle()) {
            marketLink = ServiceConstants.GOOGLE_MARKET_APP_LINK
                    .replace("{package_id}", BaseConstants.APP_PACKAGE_PRODUCTION);
        } else {
            marketLink = ServiceConstants.AMAZON_MARKET_APP_LINK
                    .replace("{package_id}", BaseConstants.APP_PACKAGE_PRODUCTION);
        }
        Uri uri = Uri.parse(marketLink);

        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);

        try {
            context.startActivity(myAppLinkToMarket);

        } catch (ActivityNotFoundException e) {
            e.printStackTrace();

            String url;

            if (!Tools.isKindle()) {
                url = ServiceConstants.GOOGLE_APP_STORE_LOCATION
                        .replace("{package_id}",
                                BaseConstants.APP_PACKAGE_PRODUCTION);
            } else {
                url = ServiceConstants.AMAZON_APP_STORE_LOCATION
                        .replace("{package_id}",
                                BaseConstants.APP_PACKAGE_PRODUCTION);
            }

            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(url));

            context.startActivity(intent);
        }
    }

    /**
     * Wherever the current device is kindle
     *
     * @return False for regular android
     */
    public static boolean isKindle() {
        return Build.MANUFACTURER.equalsIgnoreCase("Amazon");
    }

    /**
     * Converts milliseconds to seconds
     *
     * @param millis THe milliseconds to convert
     * @return The value in seconds
     */
    public static int millisecondsToSeconds(float millis) {

        double constant = 0.001;
        return (int) Math.floor(constant * millis);
    }

    /**
     * Check the volume settings to verify if audio is available
     *
     * @param context The activity's context
     * @return the volume level
     */
    public static int getAudi0Volume(Context context) {
        try {

            // check audio level
            AudioManager audioManager =
                    (AudioManager) context.getSystemService(context.AUDIO_SERVICE);

            int volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

            return volume;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    /**
     * Wherever device is marshmallow and we do not possess permissions to request location
     *
     * @return if app has permissions
     */
    public static boolean hasLocationPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= 23 &&

                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.ACCESS_FINE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        return true;
    }

    /**
     * Wherever device is marshmallow and has sd card permissions
     *
     * @return if app has permissions
     */
    public static boolean hasSdCardPermissions(Context context) {
        if (Build.VERSION.SDK_INT >= 23 &&

                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        return true;
    }

    /**
     * Wherever device is marshmallow and we do not possess permissions to write externally
     *
     * @return if app has permissions
     */
    public static boolean hasExternalWritePermissions(Context context) {
        if (Build.VERSION.SDK_INT >= 23 &&

                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {

            return false;
        }
        return true;
    }

    /**
     * Helper to open a url in external browser
     *
     * @param context The current context
     * @param url     The url address to find
     */
    public static void openExternalBrowserUrl(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    /**
     * Is view instance of control
     *
     * @param view The view
     * @return true to be instance
     */
    public static Boolean isViewInstanceOfControl(View view) {
        if (view instanceof TextView ||
                view instanceof ImageView ||
                view instanceof AppCompatSeekBar ||
                view instanceof AppCompatCheckBox) {

            return true;
        }

        return false;
    }
}