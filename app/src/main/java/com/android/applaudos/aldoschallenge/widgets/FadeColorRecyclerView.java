package com.android.applaudos.aldoschallenge.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.android.applaudos.aldoschallenge.R;

/**
 * Recycler view with custom fade color
 *
 * @author Aldo Infanzon on 4/29/2016.
 */
public class FadeColorRecyclerView extends RecyclerView {
    /* attrs example
        <declare-styleable name="CustomCheckboxView">
            <attr name="string" format="integer" />
        </declare-styleable>
    */

    /**
     * The current color
     */
    private static int mColor;

    /**
     * Class construct
     *
     * @param context The current context
     */
    public FadeColorRecyclerView(final Context context) {
        this(context, null);
    }

    /**
     * Class constructor Initializes
     *
     * @param context The current context
     * @param attrs   The attributes set
     */
    public FadeColorRecyclerView(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        // prevent exception in Android Studio / ADT interface builder
        if (this.isInEditMode()) {
            return;
        }

        final TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.ColorRecyclerView);

        mColor = array.getColor(R.styleable.ColorRecyclerView_fadeColor, -1);

        if (array != null) {
            array.recycle();
        }
    }

    /**
     * The recycler view color
     *
     * @return The solid color
     */
    @Override
    public int getSolidColor() {

        if (mColor != -1) {
            return mColor;
        }

        return super.getSolidColor();
    }
}

