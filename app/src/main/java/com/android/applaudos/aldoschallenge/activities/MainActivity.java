package com.android.applaudos.aldoschallenge.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.fragments.presenter.TeamPresenterFragment;
import com.android.applaudos.aldoschallenge.fragments.ui.TeamDetailFragment;
import com.android.applaudos.aldoschallenge.fragments.ui.TeamListFragment;
import com.android.applaudos.challenge.framework.model.database.ScheduledGames;
import com.android.applaudos.challenge.framework.model.database.Team;

public class MainActivity extends BaseActivity {

    /**
     * Key for recover if we are on the first run
     */
    private final static String KEY_IS_ON_FIRST_RUN = "isOnFirstRun";

    /**
     * Key for recover if we are on the first run
     */
    private final static String KEY_LIST_SELECTED_INDEX = "isOnFirstRun";

    /**
     * Main presenter data fragment
     */
    private TeamPresenterFragment mDataFragment;

    /**
     * Wherever we are running for the first time
     */
    private Boolean mIsOnFirstRun;

    /**
     * List of teams fragment
     */
    private TeamListFragment mTeamListFragment;

    /**
     * Team detail fragment
     */
    private TeamDetailFragment mTeamDetailFragment;

    /**
     * The home toolbar
     */
    private Toolbar mHomeToolbar;

    /**
     * The detail toolbar
     */
    private Toolbar mDetailToolbar;

    /**
     * The container for the buttons
     */
    private LinearLayout mDetailButtonContainer;

    /**
     * The share button
     */
    private RelativeLayout mShareButton;

    /**
     * The Call Button
     */
    private RelativeLayout mCallButton;

    /**
     * Back button for team detail toolbar
     */
    private RelativeLayout mBackButton;

    /**
     * The toolbar title
     */
    private TextView mToolbarTitle;

    /**
     * The Home toolbar icon
     */
    private ImageView mHomeIcon;

    /**
     * The selected team index
     */
    private int mSelectedTeamIndex;

    /**
     * On Saved instant state
     *
     * @param outState The out state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(KEY_IS_ON_FIRST_RUN, mIsOnFirstRun);
        outState.putInt(KEY_LIST_SELECTED_INDEX, mSelectedTeamIndex);

        super.onSaveInstanceState(outState);
    }

    /**
     * On Create
     *
     * @param savedInstanceState The saved instances
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setToolbars();

        // Set data fragment if not exists
        mDataFragment = (TeamPresenterFragment) getSupportFragmentManager()
                .findFragmentByTag(TeamPresenterFragment.class.getName());

        // When null we create the presenter fragment
        if (mDataFragment == null) {
            mDataFragment = TeamPresenterFragment.newInstance();

            getSupportFragmentManager().beginTransaction()
                    .add(mDataFragment, TeamPresenterFragment.class.getName())
                    .commit();
        }

        // State values
        if (savedInstanceState != null) {
            mIsOnFirstRun = savedInstanceState.getBoolean(KEY_IS_ON_FIRST_RUN, true);
            mSelectedTeamIndex = savedInstanceState.getInt(KEY_IS_ON_FIRST_RUN, 0);
        } else {
            mIsOnFirstRun = true;
            mSelectedTeamIndex = 0;
        }

        mTeamListFragment = TeamListFragment.newInstance();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            mHomeToolbar.setVisibility(View.VISIBLE);

            switchFragment(mTeamListFragment, BaseActivity.NO_ANIMATION);
        } else {
            mTeamDetailFragment = TeamDetailFragment.newInstance();

            // Placing both fragment on the view
            switchFragment(mTeamListFragment, BaseActivity.NO_ANIMATION);
            switchFragment(mTeamDetailFragment,
                    R.id.fragment_container_detail,
                    BaseActivity.NO_ANIMATION);

            mHomeToolbar.setVisibility(View.GONE);
            mDetailToolbar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * On Back Button Pressed
     */
    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT &&
                mDetailToolbar.getVisibility() == View.VISIBLE) {

            mDetailToolbar.setVisibility(View.GONE);
            mHomeToolbar.setVisibility(View.VISIBLE);

            switchFragment(mTeamListFragment, BaseActivity.FADE_ANIMATION);

            return;
        }

        super.onBackPressed();
    }

    /**
     * Helper to set toolbar and buttons
     */
    private void setToolbars() {
        mHomeToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDetailToolbar = (Toolbar) findViewById(R.id.toolbar_detail);

        mToolbarTitle = (TextView) mDetailToolbar.findViewById(R.id.title);

        mHomeIcon = (ImageView) mDetailToolbar.findViewById(R.id.home);

        mDetailButtonContainer = (LinearLayout) mDetailToolbar.findViewById(R.id.button_container);

        mCallButton = (RelativeLayout) mDetailToolbar.findViewById(R.id.call);
        mShareButton = (RelativeLayout) mDetailToolbar.findViewById(R.id.share);
        mBackButton = (RelativeLayout) mDetailToolbar.findViewById(R.id.toolbar_back);

        OnToolbarClick listener = new OnToolbarClick();

        mCallButton.setOnClickListener(listener);
        mShareButton.setOnClickListener(listener);

        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_LANDSCAPE) {

            mToolbarTitle.setText(getString(R.string.toolbar_title_applaudo_s_homework));
            mHomeIcon.setClickable(false);
            mHomeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_white_24dp));
            mBackButton.setOnClickListener(null);
        } else {
            mBackButton.setOnClickListener(listener);
        }
    }

    /**
     * Will show team detail screen
     *
     * @param team The selected team
     */
    public void showTeamDetail(Team team) {
        mTeamDetailFragment = TeamDetailFragment.newInstance(team.getId());

        mHomeToolbar.setVisibility(View.GONE);
        mDetailToolbar.setVisibility(View.VISIBLE);

        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT) {
            switchFragment(mTeamDetailFragment, BaseActivity.FADE_ANIMATION);
        } else {
            switchFragment(mTeamDetailFragment,
                    R.id.fragment_container_detail,
                    BaseActivity.NO_ANIMATION);
        }
    }

    /**
     * Will show toolbar buttons
     */
    public void showToolbarButtons() {
        mDetailButtonContainer.setVisibility(View.VISIBLE);
    }

    /**
     * The main Presenter data fragment the current activity
     */
    public TeamPresenterFragment getDataFragment() {
        return mDataFragment;
    }

    /**
     * The list of teams fragment
     *
     * @return The fragment of teams list
     */
    public TeamListFragment getTeamList() {
        return mTeamListFragment;
    }

    /**
     * Wherever we are running for the first time
     *
     * @return true for first run
     */
    public Boolean isOnFirstRun() {
        return mIsOnFirstRun;
    }

    /**
     * Set wherever we are running the first run
     *
     * @param isOnFirstRun true for be at the first run
     */
    public void setIsOnFirstRun(Boolean isOnFirstRun) {
        this.mIsOnFirstRun = isOnFirstRun;
    }

    /**
     * The selected team index
     *
     * @return List selected index value
     */
    public int getSelectedTeamIndex() {
        return mSelectedTeamIndex;
    }

    /**
     * Sets the current team index
     *
     * @param selectedTeamIndex The index of the list to be set
     */
    public void setSelectedTeamIndex(int selectedTeamIndex) {
        this.mSelectedTeamIndex = selectedTeamIndex;
    }

    /**
     * Team detail fragment
     * @return The Team detail fragment
     */
    public TeamDetailFragment getTeamDetailFragment() {
        return mTeamDetailFragment;
    }

    /**
     * On Toolbar Button Clicked
     */
    private class OnToolbarClick implements View.OnClickListener {

        /**
         * When a toolbar button clicks
         *
         * @param view The clicked view
         */
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.call:

                    String phone = mTeamDetailFragment
                            .getCurrentTeam().getPhoneNumber();

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));

                    startActivity(intent);

                    break;

                case R.id.share:

                    Team team = mTeamDetailFragment.getCurrentTeam();

                    ScheduledGames game = mDataFragment.getGame(team.getId());

                    // If no next games just show an error
                    if (game == null) {

                        Toast.makeText(MainActivity.this, R.string.no_next_games,
                                Toast.LENGTH_LONG).show();

                        return;
                    }

                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TITLE, team.getTeamName() + " " +
                            getString(R.string.next_game));
                    sendIntent.putExtra(Intent.EXTRA_TEXT, team.getNickname() +
                            "@" +
                            game.getStadium() +
                            " " + game.getDate());
                    sendIntent.setType("text/plain");

                    startActivity(Intent.createChooser(sendIntent,
                            getResources().getText(R.string.share)));

                    break;

                case R.id.toolbar_back:

                    if (getResources().getConfiguration()
                            .orientation == Configuration.ORIENTATION_PORTRAIT) {

                        mDetailToolbar.setVisibility(View.GONE);
                        mHomeToolbar.setVisibility(View.VISIBLE);

                        switchFragment(mTeamListFragment, BaseActivity.FADE_ANIMATION);
                    }
                    break;

                default:
                    break;
            }
        }
    }
}
