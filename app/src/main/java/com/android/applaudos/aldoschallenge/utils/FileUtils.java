package com.android.applaudos.aldoschallenge.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.FileChannel;

/**
 * String utilities class
 *
 * @author Aldo on 1/22/2016.
 */
public class FileUtils {

    /**
     * Clear files from a given folder
     *
     * @param folderPath The folder path location to search for files to delete
     */
    public static void clearFolderFiles(String folderPath) {

        File files[] = listFolderFiles(folderPath);

        if (files == null || files.length < 1) {
            Log.w("IO Warning:", "No files in folder");

            return;
        }

        for (int i = 0; i < files.length; i++) {
            files[i].delete();
        }
    }

    /**
     * List all files in given folder
     *
     * @param folderPath The folder path to search for files
     * @return Files array
     */
    public static File[] listFolderFiles(String folderPath) {
        File folder = new File(folderPath);

        if (!folder.exists()) {
            Log.e("IO Error:", "No such Folder");

            return null;
        }

        File files[] = folder.listFiles();

        return files;
    }

    /**
     * Convert Stream to String
     *
     * @param inputStream The input stream
     * @return converted stream into string
     * @throws Exception File Exception on error
     */
    public static String convertStreamToString(InputStream inputStream) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder stringBuilder = new StringBuilder();

        String line;

        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }

        reader.close();

        return stringBuilder.toString();
    }

    /**
     * Get string from file
     *
     * @param filePath The file path
     * @return The file text content
     */
    public static String getStringFromFile(String filePath) {
        String fileText = null;

        try {
            File file = new File(filePath);

            FileInputStream fileInputStream = new FileInputStream(file);

            fileText = convertStreamToString(fileInputStream);

            //Make sure you close all streams.
            fileInputStream.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return fileText;
    }

    /**
     * Moves a file
     *
     * @param inputFilePath The file to move to another location
     * @param outputPath    The folder route where to place file
     * @param offset        The offset to start copying the file
     * @param deleteSource  True to delete src file after copy
     * @return moved file object
     */
    public static boolean moveFile(String inputFilePath,
                                   String outputPath,
                                   int offset,
                                   boolean deleteSource) {

        FileChannel outputChannel = null;
        FileChannel inputChannel = null;

        try {

            outputChannel = new FileOutputStream(outputPath).getChannel();
            inputChannel = new FileInputStream(inputFilePath).getChannel();
            inputChannel.transferTo(offset, inputChannel.size(), outputChannel);
            inputChannel.close();

            if (deleteSource) {
                File file = new File(inputFilePath);
                if (file.exists()) {
                    file.delete();
                }
            }

            return true;

        } catch (Exception ex) {
            return false;
        } finally {
            try {
                if (inputChannel != null) inputChannel.close();
                if (outputChannel != null) outputChannel.close();
            } catch (Exception ex) {

            }
        }
    }

    /**
     * Returns the absolute path from the local directory where local files are stored
     *
     * @param context The app context
     * @return The local absolute path
     */
    public static String getLocalAbsolutePath(Context context) {
        return context.getFilesDir().getAbsolutePath();
        //return context.getExternalFilesDir(null).getAbsolutePath();
    }

    /**
     * Gets the users folder from the old app
     *
     * @param context The activity context
     * @return The old user folder
     */
    public static String getUsersLocalFolder(Context context) {
        String localPath = context.getFilesDir().getAbsolutePath() + File.separator + "users";
        return localPath;
    }

    /**
     * Gets the old users external folder
     *
     * @param context The activity context
     * @return The user folder
     */
    public static String getUsersExternalFolder(Context context) {
        String localUserPath = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                .getAbsolutePath() + File.separator + "users";

        return localUserPath;
    }

    /**
     * Gets the users SD Card directory if it exists
     */
    public static String getUserSDCardDirectory(File rootDir) {

        if (rootDir != null && rootDir.exists() && rootDir.isDirectory()) {
            return rootDir.getAbsolutePath() + File.separator + "users";
        }
        return null;
    }

    /**
     * Reads all content from a file
     *
     * @param context The activity context
     * @return The content of file as string
     */
    public static String readFromFile(Context context, String filepath) {

        String ret = "";
        FileInputStream fis = null;

        try {
            fis = new FileInputStream(new File(filepath));
            InputStreamReader inputStream = new InputStreamReader(fis);

            if (inputStream != null) {
                StringBuilder sb = new StringBuilder();
                char[] inputBuffer = new char[2048];
                int l;
                while ((l = inputStream.read(inputBuffer)) != -1) {
                    sb.append(inputBuffer, 0, l);
                }
                ret = sb.toString();
                fis.close();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        } finally {
            if (fis != null) {
                fis = null;
            }
        }
        return ret;
    }
}