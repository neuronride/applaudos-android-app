package com.android.applaudos.aldoschallenge.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.android.applaudos.aldoschallenge.R;

/**
 * Custom checkbox class
 * Add Styleables in attrs
 *
 * @author Aldo Infanzon on 8/16/2016.
 */
public class ColorProgressBar extends ProgressBar {

    /* attrs example
    <declare-styleable name="CustomCheckboxView">
        <attr name="string" format="integer" />
    </declare-styleable>
    */

    /**
     * The current color
     */
    private static int mColor;

    /**
     * Class constructor. Initializes default values
     *
     * @param context The current context
     * @param attrs   The attributes
     */
    public ColorProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);

        // prevent exception in Android Studio / ADT interface builder
        if (this.isInEditMode()) {
            return;
        }

        final TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.CustomProgressColor);

        // prevent exception in Android Studio / ADT interface builder
        if (this.isInEditMode()) {
            return;
        }

        mColor = array.getColor(R.styleable.CustomProgressColor_color, -1);

        if (array != null) {
            array.recycle();
        }

        if (mColor != -1) {

            Drawable currentDrawable = this.getProgressDrawable();

            if (currentDrawable == null) {
                currentDrawable = this.getIndeterminateDrawable();
            }

            currentDrawable.setColorFilter(
                    mColor, android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }
}
