package com.android.applaudos.aldoschallenge.fragments.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.utils.StringUtil;

/**
 * Error Dialog Fragment with simple ok button
 *
 * @author Aldo Infanzon on 11/09/2016.
 */
public class ErrorDialogFragment extends BaseDialogFragment {

    /**
     * Button Text
     */
    public final static String BUTTON_TEXT = "buttonText";

    /**
     * Message save tag
     */
    public final static String MESSAGE = "message";

    /**
     * Title save tag
     */
    public final static String TITLE = "title";

    /**
     * Ok Button
     */
    private View mOkButton;

    /**
     * Text View for the button
     */
    private TextView mButtonText;

    /**
     * Text View message
     */
    private TextView mDescription;

    /**
     * Text View for the dialog
     */
    private TextView mTitle;

    /**
     * The root view
     */
    private View mRootView;

    /**
     * The external click handler
     */
    private View.OnClickListener mExternalClick;

    /**
     * Creates a new instance for error dialog
     *
     * @param title      The title
     * @param message    The message
     * @param buttonText The text button
     * @return The dialog instance
     */
    public static ErrorDialogFragment newInstance(String title,
                                                  String message,
                                                  String buttonText) {
        ErrorDialogFragment fragment = new ErrorDialogFragment();

        Bundle args = new Bundle();
        args.putString(MESSAGE, message);
        args.putString(TITLE, title);
        args.putString(BUTTON_TEXT, buttonText);

        fragment.setArguments(args);

        return fragment;
    }

    /**
     * On Create Dialog
     *
     * @param savedInstanceState The saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    /**
     * On Create View event
     *
     * @param inflater           The layout inflater
     * @param container          The view group container
     * @param savedInstanceState The saved instances for fragment
     * @return main view holder
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mRootView == null) {

            mRootView = inflater.inflate(
                    R.layout.dialog_fragment_error,
                    container,
                    false);

            mDescription = (TextView) mRootView.findViewById(R.id.description);
            mTitle = (TextView) mRootView.findViewById(R.id.title);

            mButtonText = (TextView) mRootView.findViewById(R.id.button_text);
            mOkButton = mRootView.findViewById(R.id.ok_button);
            if (mExternalClick == null) {

                mOkButton.setOnClickListener(new DialogListener());
            } else {
                mOkButton.setOnClickListener(mExternalClick);
            }

            // Make the user upgrade by not letting him cancel the dialog
            setCancelable(false);

            if (getArguments() != null) {
                String message = getArguments().getString(MESSAGE, null),
                        title = getArguments().getString(TITLE, null),
                        buttonTitle = getArguments().getString(BUTTON_TEXT, null);

                if (!StringUtil.stringIsNullOrEmpty(message)) {
                    mDescription.setText(message);
                } else {
                    mDescription.setVisibility(View.GONE);
                }

                if (!StringUtil.stringIsNullOrEmpty(title)) {
                    mTitle.setText(title);
                } else {
                    mTitle.setVisibility(View.GONE);
                }

                if (!StringUtil.stringIsNullOrEmpty(buttonTitle)) {
                    mButtonText.setText(buttonTitle);
                } else {
                    mButtonText.setText(getActivity().getString(R.string.ok));
                }
            }
        }

        return mRootView;
    }

    /**
     * When saved instance is reach
     *
     * @param outState The out state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * On Destroy hack to avoid destroying dialog on orientation change
     */
    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);

        super.onDestroyView();
    }

    /**
     * Sets ok button listener in case the user needs it
     *
     * @param listener The listener
     * @return The view id
     */
    public int setOkButtonListener(View.OnClickListener listener) {

        mExternalClick = listener;

        return R.id.ok_button;
    }

    /**
     * Click helper class for go to player
     */
    private class DialogListener implements View.OnClickListener {

        /**
         * Controls on click
         *
         * @param view The view button that was clicked
         */
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.ok_button:
                    ErrorDialogFragment.this.dismiss();
                    break;

                default:
                    break;
            }
        }
    }
}