package com.android.applaudos.aldoschallenge.fragments.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.applaudos.aldoschallenge.activities.BaseActivity;

/**
 * Login Data Fragment Class Presenter
 *
 * @author Aldo Infanzon on 4/18/2016.
 */
public class BasePresenterFragment extends Fragment {

    /**
     * The Fragment Name
     *
     * @return The fragment name
     */
    public String getTagName() {
        return this.getClass().getName();
    }

    /**
     * Base Activity
     */
    private BaseActivity mBaseActivity;

    /**
     * On Create Presenter Fragment
     *
     * @param savedInstanceState The saved instance
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    /**
     * On Fragment Create View. Returns null view
     * </b>
     * Fragment should have a null view
     *
     * @param inflater           The layout inflater
     * @param container          The parent view group container
     * @param savedInstanceState The saved instances
     * @return The Root Fragment root view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return null;
    }

    /**
     * On Resume
     */
    @Override
    public void onResume() {
        super.onResume();

        if (mBaseActivity == null) {
            mBaseActivity = (BaseActivity) getActivity();
        }
    }

    /**
     * The current base activity
     */
    public BaseActivity getBaseActivity() {
        if (mBaseActivity == null) {
            mBaseActivity = (BaseActivity) getActivity();
        }

        return mBaseActivity;
    }
}