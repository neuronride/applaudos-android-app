package com.android.applaudos.aldoschallenge.widgets;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import com.android.applaudos.aldoschallenge.R;

import java.util.HashMap;
import java.util.Map;

/**
 * The font face text view
 *
 * @author Aldo Infanzon on 4/29/2016.
 */
public class FontFaceAutoCompleteTextView extends AutoCompleteTextView {

    /*
     * Caches typefaces based on their file path and name, so that they don't have to be created
     * every time when they are referenced.
     */
    private static Map<String, Typeface> mTypefaces;

    /**
     * The scheme where to take value
     */
    private static final String SCHEME = "http://schemas.android.com/apk/res-auto";

    /**
     * The scheme where to take value
     */
    private static final String FONT_FACE = "type_face";

    /**
     * Class construct
     *
     * @param context The current context
     */
    public FontFaceAutoCompleteTextView(final Context context) {
        this(context, null);
    }

    /**
     * Class constructor Initializes
     *
     * @param context The current context
     * @param attrs   The attributes set
     */
    public FontFaceAutoCompleteTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        if (mTypefaces == null) {
            mTypefaces = new HashMap<>();
        }

        // prevent exception in Android Studio / ADT interface builder
        if (this.isInEditMode()) {
            return;
        }

        final TypedArray array = context.obtainStyledAttributes(attrs,
                R.styleable.TypefaceTextView);

        if (array != null) {
            final String typefaceAssetPath = attrs.getAttributeValue(
                    SCHEME, FONT_FACE);

            if (typefaceAssetPath != null) {
                Typeface typeface;

                try {
                    if (mTypefaces.containsKey(typefaceAssetPath)) {
                        typeface = mTypefaces.get(typefaceAssetPath);

                        setTypeface(typeface);
                    } else {
                        AssetManager assets = context.getAssets();
                        typeface = Typeface.createFromAsset(assets, typefaceAssetPath);
                        mTypefaces.put(typefaceAssetPath, typeface);

                        setTypeface(typeface);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            array.recycle();
        }
    }
}
