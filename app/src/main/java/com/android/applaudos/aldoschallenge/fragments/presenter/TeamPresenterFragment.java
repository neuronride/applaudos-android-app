package com.android.applaudos.aldoschallenge.fragments.presenter;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.android.applaudos.aldoschallenge.AldosChallengeApplication;
import com.android.applaudos.aldoschallenge.activities.MainActivity;
import com.android.applaudos.challenge.framework.business.games.GamesBusiness;
import com.android.applaudos.challenge.framework.business.teams.TeamBusiness;
import com.android.applaudos.challenge.framework.model.database.ScheduledGames;
import com.android.applaudos.challenge.framework.model.database.Team;
import com.raizlabs.android.dbflow.sql.language.CursorResult;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Data Presenter Persistent Fragment
 *
 * @author aldoinfanzon on 4/1/2017.
 */
public class TeamPresenterFragment extends BasePresenterFragment {
    /**
     * Team Business
     */
    private TeamBusiness mTeamBusiness;

    /**
     * Game Business
     */
    private GamesBusiness mScheduledBusiness;

    /**
     * Creates a new fragment instance
     *
     * @return The new fragment instance
     */
    public static TeamPresenterFragment newInstance() {
        Bundle args = new Bundle();

        TeamPresenterFragment fragment = new TeamPresenterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Gets the current logged in user
     *
     * @param id The Team id
     * @return The team by its id
     */
    public Team getTeam(int id) {
        return getTeamBusiness().getTeamDb(id);
    }

    /**
     * Gets the current logged in user
     *
     * @param id The Team id
     * @return The last game in order
     */
    public ScheduledGames getGame(int id) {
        return getGamesBusiness().
                getLastGame(id);
    }

    /**
     * Get menu from service
     *
     * @return Call executed
     */
    public Call getTeams() {
        return getTeamBusiness().getTeams(new OnTeamResponse());
    }

    /**
     * Get current db teams
     */
    public void getDbTeamsAsync() {
        getTeamBusiness().getDbTeamsAsync(new OnDbTeamsListCallback());
    }

    /**
     * Get current db teams
     *
     * @return The list of teams
     */
    public List<Team> getDbTeams() {
        return getTeamBusiness().getDbTeams();
    }

    /**
     * Team Business
     *
     * @return The business class
     */
    private TeamBusiness getTeamBusiness() {

        if (mTeamBusiness == null) {

            mTeamBusiness = new TeamBusiness(
                    AldosChallengeApplication.getAppInstance(),
                    AldosChallengeApplication.okHttpClient);
        }

        return mTeamBusiness;
    }


    /**
     * Games Business
     *
     * @return The business class
     */
    private GamesBusiness getGamesBusiness() {

        if (mScheduledBusiness == null) {

            mScheduledBusiness = new GamesBusiness();
        }

        return mScheduledBusiness;
    }

    /**
     * On Db Items Callback
     */
    private class OnDbTeamsListCallback implements QueryTransaction.QueryResultCallback<Team> {
        /**
         * On Items Callback
         */
        OnDbTeamsListCallback() {
        }

        /**
         * On Query Response
         *
         * @param transaction The transaction
         * @param tResult     The result
         */
        @Override
        public void onQueryResult(QueryTransaction<Team> transaction,
                                  @NonNull CursorResult<Team> tResult) {

            if (getBaseActivity() == null) {
                return;
            }

            final List<Team> teamList = tResult.toList();

            getBaseActivity().runOnUiThread(new Runnable() {
                /**
                 * Run answer on ui thread
                 */
                @Override
                public void run() {
                    ((MainActivity) getActivity()).getTeamList()
                            .onUpdateTeamsSuccess(teamList);

                    if (getBaseActivity().getResources().getConfiguration().orientation ==
                            Configuration.ORIENTATION_LANDSCAPE &&
                            teamList.size() > 0) {

                        ((MainActivity) getActivity()).getTeamDetailFragment()
                                .initializeDetailView(teamList.get(0).getId());
                    }
                }
            });
        }
    }

    /**
     * On Service Success Class holder
     */
    private class OnTeamResponse implements Callback {

        /**
         * When Service has an error
         *
         * @param call The call
         * @param e    The IO Exception
         */
        @Override
        public void onFailure(okhttp3.Call call, IOException e) {
            if (getActivity() == null || call.isCanceled()) {
                return;
            }

            getBaseActivity().runOnUiThread(new Runnable() {
                /**
                 * Run answer on ui thread
                 */
                @Override
                public void run() {
                    ((MainActivity) getActivity()).getTeamList()
                            .onUpdateTeamsError(null);
                }
            });

        }

        /**
         * On Response
         *
         * @param call     The call
         * @param response The response
         * @throws IOException
         */
        @Override
        public void onResponse(okhttp3.Call call,
                               Response response) throws IOException {
            if (getActivity() == null || call.isCanceled()) {
                return;
            }

            final List<Team> teamList = getDbTeams();

            final int responseCode = response.code();

            getBaseActivity().runOnUiThread(new Runnable() {
                /**
                 * Run answer on ui thread
                 */
                @Override
                public void run() {
                    switch (responseCode) {
                        case 200:

                            ((MainActivity) getActivity()).getTeamList()
                                    .onUpdateTeamsSuccess(teamList);

                            if (getBaseActivity().getResources().getConfiguration().orientation ==
                                    Configuration.ORIENTATION_LANDSCAPE &&
                                    teamList.size() > 0) {

                                ((MainActivity) getActivity()).getTeamDetailFragment()
                                        .initializeDetailView(teamList.get(0).getId());
                            }

                            break;

                        default:

                            ((MainActivity) getActivity()).getTeamList()
                                    .onUpdateTeamsError(null);
                            break;
                    }
                }
            });
        }
    }
}