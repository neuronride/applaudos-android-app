package com.android.applaudos.aldoschallenge.fragments.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.activities.MainActivity;

/**
 * Splash Screen Fragment
 */
public class SplashFragment extends BaseFragment {

    /**
     * Class Constructor
     */
    public SplashFragment() {
        // Required empty public constructor
    }

    /**
     * Creates a new instance
     *
     * @return The Fragment instance
     */
    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();

        Bundle args = new Bundle();

        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Wherever is the first instance
     *
     * @param outState The out state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * On Fragment creation
     *
     * @param savedInstanceState The current saved instances
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * On Fragment create view
     *
     * @param inflater           The inflater
     * @param container          The container
     * @param savedInstanceState The saved instances
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_splash, container, false);

        return rootView;
    }

    /**
     * On View Resume
     */
    @Override
    public void onResume() {
        super.onResume();

        getBaseActivity().setCurrentFragment(this);

        launchMainActivity();
    }


    /**
     * When Services ends up loading it will hit this method
     */
    public void launchMainActivity() {

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            /**
             * Makes some time to run animation
             */
            @Override
            public void run() {
                if (getActivity() == null) {
                    return;
                }

                Intent mainIntent = new Intent(getActivity(), MainActivity.class);

                getActivity().startActivity(mainIntent);
                getActivity().finish();
            }
        }, 2000);
    }
}
