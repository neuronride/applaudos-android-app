package com.android.applaudos.aldoschallenge.fragments.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.activities.MainActivity;
import com.android.applaudos.aldoschallenge.player.PlayerController;
import com.android.applaudos.aldoschallenge.utils.StringUtil;
import com.android.applaudos.challenge.framework.model.database.Team;
import com.bumptech.glide.Glide;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Splash Screen Fragment
 */
public class TeamDetailFragment extends BaseFragment {

    /**
     * Key for recover team id
     */
    private final static String KEY_TEAM_ID = "teamId";

    /**
     * Player instance class
     */
    private PlayerController mPlayerInstance;

    /**
     * The Loading progress bar
     */
    private ProgressBar mProgressBar;

    /**
     * The map fragment
     */
    private MapView mMap;

    /**
     * The Player View
     */
    private EMVideoView mPlayerView;

    /**
     * The google map
     */
    private GoogleMap mGoogleMap;

    /**
     * The team id
     */
    private int mTeamId;

    /**
     * The current team
     */
    private Team mCurrentTeam;

    /**
     * The team name text view
     */
    private TextView mTitle;

    /**
     * The team description
     */
    private TextView mDescription;

    /**
     * The team image
     */
    private ImageView mImage;

    /**
     * Class Constructor
     */
    public TeamDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Creates a new instance
     *
     * @param id The Team id
     * @return The Fragment instance
     */
    public static TeamDetailFragment newInstance(int id) {
        TeamDetailFragment fragment = new TeamDetailFragment();

        Bundle args = new Bundle();

        args.putInt(KEY_TEAM_ID, id);

        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Creates a new instance
     *
     * @return The Fragment instance
     */
    public static TeamDetailFragment newInstance() {
        TeamDetailFragment fragment = new TeamDetailFragment();

        Bundle args = new Bundle();

        fragment.setArguments(args);

        return fragment;
    }

    /**
     * Wherever is the first instance
     *
     * @param outState The out state
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_TEAM_ID, mTeamId);

        super.onSaveInstanceState(outState);
    }

    /**
     * On Fragment creation
     *
     * @param savedInstanceState The current saved instances
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mTeamId = savedInstanceState.getInt(KEY_TEAM_ID, -1);
        } else {
            mTeamId = getArguments().getInt(KEY_TEAM_ID, -1);
        }
    }

    /**
     * On Fragment create view
     *
     * @param inflater           The inflater
     * @param container          The container
     * @param savedInstanceState The saved instances
     * @return the view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_team_detail, container, false);

        mTitle = (TextView) rootView.findViewById(R.id.title);
        mDescription = (TextView) rootView.findViewById(R.id.description);

        mPlayerView = (EMVideoView) rootView.findViewById(R.id.video_view);

        mImage = (ImageView) rootView.findViewById(R.id.image);

        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progress_bar);

        MapsInitializer.initialize(getBaseActivity());

        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity())) {
            case ConnectionResult.SUCCESS:
                mMap = (MapView) rootView.findViewById(R.id.map);
                mMap.onCreate(savedInstanceState);

            default:
                break;
        }

        return rootView;
    }

    /**
     * On View Resume
     */
    @Override
    public void onResume() {
        super.onResume();

        mMap.onResume();

        getBaseActivity().setCurrentFragment(this);

        if (mTeamId > -1) {
            initializeDetailView(mTeamId);
        }
    }

    /**
     * On pause
     */
    @Override
    public void onPause() {
        super.onPause();

        if (mPlayerInstance != null &&
                mPlayerView != null) {

            mPlayerView.pause();
        }
    }

    /**
     * On Fragment destroy
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.onDestroy();
    }

    /**
     * On Low Memory
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMap.onLowMemory();
    }

    /**
     * Initializes detail when information is ready
     *
     * @param teamId The team id
     */
    public void initializeDetailView(int teamId) {
        mTeamId = teamId;

        mCurrentTeam = ((MainActivity) getBaseActivity())
                .getDataFragment().getTeam(mTeamId);

        if (!StringUtil.stringIsNullOrEmpty(mCurrentTeam.getTeamName())) {
            mTitle.setText(mCurrentTeam.getTeamName());
        }

        if (!StringUtil.stringIsNullOrEmpty(mCurrentTeam.getDescription())) {
            mDescription.setText(mCurrentTeam.getDescription());
        }

        if (!StringUtil.stringIsNullOrEmpty(mCurrentTeam.getLogoImage())) {

            Glide.with(getBaseActivity())
                    .load(mCurrentTeam.getLogoImage())
                    .placeholder(getBaseActivity()
                            .getResources().getDrawable(R.drawable.ic_camera))
                    .error(getBaseActivity()
                            .getResources().getDrawable(R.drawable.ic_no_image))
                    .override(300, 400)
                    .centerCrop()
                    .into(mImage);
        } else {

            Glide.with(getBaseActivity())
                    .load("")
                    .placeholder(getBaseActivity()
                            .getResources().getDrawable(R.drawable.ic_camera))
                    .fitCenter()
                    .into(mImage);
        }

        // Gets to GoogleMap from the MapView and does initialization stuff
        if (mGoogleMap == null) {
            mMap.getMapAsync(new OnMapReady());
        }

        // If there is video then let's run it
        if (!StringUtil.stringIsNullOrEmpty(mCurrentTeam.getVideoUrl())) {
            getPlayerInstance().play(mCurrentTeam.getVideoUrl());
        }

        // Let's show buttons only when became available
        ((MainActivity) getBaseActivity()).showToolbarButtons();
    }

    /**
     * The current team
     *
     * @return The current selected team
     */
    public Team getCurrentTeam() {
        return mCurrentTeam;
    }

    /**
     * The Player controller getter
     *
     * @return The instance
     */
    private PlayerController getPlayerInstance() {
        if (mPlayerInstance == null) {
            mPlayerInstance = new PlayerController(mPlayerView);
        }

        return mPlayerInstance;
    }

    /**
     * Class to handle map ready event
     */
    private class OnMapReady implements OnMapReadyCallback {
        /**
         * When map is ready will start in here
         *
         * @param googleMap The google map
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {

            mGoogleMap = googleMap;

            if (mCurrentTeam != null &&
                    mCurrentTeam.getLatitude() != null &&
                    mCurrentTeam.getLatitude() != null) {

                LatLng teamLocation = new LatLng(
                        mCurrentTeam.getLatitude(),
                        mCurrentTeam.getLongitude());

                MarkerOptions markerOptions = new MarkerOptions()
                        .position(teamLocation)
                        .title(mCurrentTeam.getTeamName());

                googleMap.addMarker(markerOptions);

                CameraUpdate cameraUpdate = CameraUpdateFactory
                        .newLatLngZoom(teamLocation, 9);

                mGoogleMap.animateCamera(cameraUpdate);
            }

            mProgressBar.setVisibility(View.GONE);
        }
    }
}
