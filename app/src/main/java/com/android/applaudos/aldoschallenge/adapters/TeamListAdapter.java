package com.android.applaudos.aldoschallenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.activities.MainActivity;
import com.android.applaudos.aldoschallenge.utils.StringUtil;
import com.android.applaudos.challenge.framework.model.database.Team;
import com.bumptech.glide.Glide;
import com.raizlabs.android.dbflow.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Order List Menu Item Adapter class
 *
 * @author Aldo Infanzon on 11/27/2016.
 */
public class TeamListAdapter extends RecyclerView.Adapter<TeamListAdapter.ViewHolder> {

    /**
     * The Current Context
     */
    private Context mContext;

    /**
     * The data for this list
     */
    private List<Team> mItemList;

    /**
     * The Adapter class constructor
     *
     * @param context The current context
     */
    public TeamListAdapter(Context context) {
        mContext = context;

        mItemList = new ArrayList<>();
    }

    /**
     * On Create view
     *
     * @param parent   The parent view group
     * @param viewType The view type
     * @return View Holder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_row_team, parent, false);

        return new ViewHolder(view);
    }

    /**
     * On Bind View controls
     *
     * @param holder   the view holder
     * @param position The position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Team item = mItemList.get(position);

        if (!StringUtils.isNullOrEmpty(item.getTeamName())) {
            holder.title.setText(item.getTeamName());
        }

        if (!StringUtils.isNullOrEmpty(item.getAddress())) {
            holder.subtitle.setText(item.getAddress());
        }

        if (!StringUtil.stringIsNullOrEmpty(item.getLogoImage())) {

            Glide.with(mContext)
                    .load(item.getLogoImage())
                    .placeholder(mContext.getResources().getDrawable(R.drawable.ic_camera))
                    .error(mContext.getResources().getDrawable(R.drawable.ic_no_image))
                    .override(200, 200)
                    .centerCrop()
                    .into(holder.image);
        } else {

            Glide.with(mContext)
                    .load("")
                    .placeholder(mContext.getResources().getDrawable(R.drawable.ic_camera))
                    .fitCenter()
                    .into(holder.image);
        }
    }

    /**
     * Get The Total Item count
     *
     * @return The item data size
     */
    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    /**
     * Updates current items
     *
     * @param itemList The item list
     */
    public void updateItemList(List<Team> itemList) {
        mItemList = itemList;

        notifyDataSetChanged();
    }

    /**
     * The data for this list
     *
     * @return The data of the adapter
     */
    public List<Team> getMenuItemList() {
        return mItemList;
    }

    /**
     * Recycler View Holder class
     */
    protected class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        /**
         * The clickable container
         */
        protected View container;

        /**
         * Row Image
         */
        protected ImageView image;

        /**
         * The main title
         */
        protected TextView title;

        /**
         * The item subtitle
         */
        protected TextView subtitle;

        /**
         * The item total
         */
        protected TextView total;

        /**
         * The class constructor, initializes view holder
         *
         * @param itemView The item view
         */
        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.title);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);

            image = (ImageView) itemView.findViewById(R.id.image);

            container = itemView.findViewById(R.id.container);
            container.setOnClickListener(this);
        }

        /**
         * On View Click
         *
         * @param view The View
         */
        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();

            Team item = mItemList.get(position);

            ((MainActivity) mContext).showTeamDetail(item);
        }
    }
}
