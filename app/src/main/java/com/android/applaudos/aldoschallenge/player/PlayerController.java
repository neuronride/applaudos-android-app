package com.android.applaudos.aldoschallenge.player;

import android.net.Uri;

import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;

/**
 * Player Controller Class
 * @author aldoinfanzon on 4/2/2017
 */
public class PlayerController implements OnPreparedListener{

    /**
     * The player view
     */
    private EMVideoView mPlayer;

    /**
     * Initializes Player Values
     * @param playerView The video view
     */
    public PlayerController(EMVideoView playerView) {
        mPlayer = playerView;
    }

    /**
     * Play Video
     */
    public void play(String url) {
        mPlayer.setOnPreparedListener(this);

        // Simple play
        mPlayer.setVideoURI(Uri.parse(url));
    }


    @Override
    public void onPrepared() {
        mPlayer.start();
    }
}
