package com.android.applaudos.aldoschallenge.utils;


import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Geo locations com.gueroalteno.android.order.utils
 *
 * @author Aldo Infanzon on 7/15/2016.
 */
public class GeoLocationUtils implements LocationListener {
    /**
     * Tag for logger
     */
    static private final String LOG_TAG = "GeoLocationUtils";

    /**
     * Minimum time between updates in milliseconds
     */
    static private final int TIME_INTERVAL = 60000;

    /**
     * Minimum distance between updates in meters
     */
    static private final int DISTANCE_INTERVAL = 1;

    /**
     * Method type to request location
     */
    public enum Method {
        NETWORK,
        GPS,
        NETWORK_THEN_GPS
    }

    /**
     * The current context
     */
    private Context mContext;

    /**
     * The location manager
     */
    private LocationManager mLocationManager;

    /**
     * The location method
     */
    private Method mMethod;

    /**
     * Callback for listening events
     */
    private Listener mCallback;

    /**
     * Class constructor
     *
     * @param context Initializes with context
     */
    public GeoLocationUtils(Context context) {
        super();
        mContext = context;
        mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * On Location changed
     *
     * @param location The location
     */
    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onLocationChanged(Location location) {
        Log.d(LOG_TAG, "Location found : " + location.getLatitude() + ", " +
                location.getLongitude() + (location.hasAccuracy() ? " : +- " +
                location.getAccuracy() + " meters" : ""));

        mLocationManager.removeUpdates(this);

        mCallback.onLocationFound(location);
    }

    /**
     * When provider has been disabled
     *
     * @param provider The location provider
     */
    @SuppressWarnings({"MissingPermission"})
    @Override
    public void onProviderDisabled(String provider) {
        Log.d(LOG_TAG, "Provider disabled : " + provider);
        if (mMethod == Method.NETWORK_THEN_GPS
                && provider.contentEquals(LocationManager.NETWORK_PROVIDER)) {

            // Network provider disabled, try GPS
            Log.d(LOG_TAG, "Request updates from GPS provider, network provider disabled.");

            requestUpdates(LocationManager.GPS_PROVIDER);
        } else {

            if (Tools.hasLocationPermissions(mContext)) {

                mLocationManager.removeUpdates(this);

                mCallback.onLocationNotFound();
            }
        }
    }

    /**
     * When provider has been enabled
     *
     * @param provider The provider
     */
    @Override
    public void onProviderEnabled(String provider) {
        Log.d(LOG_TAG, "Provider enabled : " + provider);
    }

    /**
     * On Status Changed
     *
     * @param provider The provider
     * @param status   The status
     * @param extras   Extras
     */
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(LOG_TAG, "Provided status changed : " + provider + " : status : " + status);
    }

    /**
     * Method and callback to get location
     *
     * @param method   Method to request location
     * @param callback The callback
     */
    @SuppressWarnings({"MissingPermission"})
    public void requestLocation(GeoLocationUtils.Method method,
                                GeoLocationUtils.Listener callback) {
        mMethod = method;

        mCallback = callback;

        switch (mMethod) {
            case NETWORK:
            case NETWORK_THEN_GPS:

                Location networkLocation = mLocationManager
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                if (networkLocation != null) {
                    Log.d(LOG_TAG, "Last known location found for network provider : " +
                            networkLocation.toString());

                    mCallback.onLocationFound(networkLocation);
                } else {

                    Log.d(LOG_TAG, "Request updates from network provider.");

                    requestUpdates(LocationManager.NETWORK_PROVIDER);
                }
                break;
            case GPS:
                if (!Tools.hasLocationPermissions(mContext)) {
                    return;
                }

                Location gpsLocation = mLocationManager
                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if (gpsLocation != null) {
                    Log.d(LOG_TAG, "Last known location found for GPS provider : " +
                            gpsLocation.toString());

                    mCallback.onLocationFound(gpsLocation);
                } else {
                    Log.d(LOG_TAG, "Request updates from GPS provider.");
                    this.requestUpdates(LocationManager.GPS_PROVIDER);
                }
                break;
        }
    }

    /**
     * Requests location updates
     *
     * @param provider The provider
     */
    @SuppressWarnings({"MissingPermission"})
    private void requestUpdates(String provider) {
        if (mLocationManager.isProviderEnabled(provider)) {

            if (provider.contentEquals(LocationManager.NETWORK_PROVIDER)
                    && NetworkStateUtil.isNetworkOnline(this.mContext)) {

                Log.d(LOG_TAG, "Network connected, start listening : " + provider);

                if (!Tools.hasLocationPermissions(mContext)) {
                    return;
                }

                mLocationManager.requestLocationUpdates(
                        provider, TIME_INTERVAL, DISTANCE_INTERVAL, this);

            } else if (provider.contentEquals(LocationManager.GPS_PROVIDER)
                    && NetworkStateUtil.isConnectedMobile(this.mContext)) {

                Log.d(LOG_TAG, "Mobile network connected, start listening : " + provider);

                if (!Tools.hasLocationPermissions(mContext)) {
                    return;
                }

                mLocationManager.requestLocationUpdates(
                        provider, TIME_INTERVAL, DISTANCE_INTERVAL, this);
            } else {
                Log.d(LOG_TAG, "Proper network not connected for provider : " + provider);
                this.onProviderDisabled(provider);
            }
        } else {
            this.onProviderDisabled(provider);
        }
    }

    /**
     * Cancel constants updates
     */
    @SuppressWarnings({"MissingPermission"})
    public void cancel() {
        if (Tools.hasLocationPermissions(mContext)) {
            return;
        }

        Log.d(LOG_TAG, "Locating canceled.");
        mLocationManager.removeUpdates(this);
    }

    /**
     * Listener to know if location was found or not
     */
    public interface Listener {
        /**
         * On location found
         *
         * @param location The location
         */
        void onLocationFound(Location location);

        /**
         * When we do not access to location
         */
        void onLocationNotFound();
    }
}