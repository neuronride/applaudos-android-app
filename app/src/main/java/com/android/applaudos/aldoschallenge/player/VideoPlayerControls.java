package com.android.applaudos.aldoschallenge.player;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.AppCompatSeekBar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.utils.StringUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Video Player Controls Base Class
 *
 * @author Aldo Infanzon on 5/30/2016
 * @see PlayerController Where business logic is been applied
 */
public class VideoPlayerControls {

    /**
     * Timeout for timer on post delayed for controls
     */
    private static final int CONTROLS_TIMEOUT = 3000;

    /**
     * Runnable to control remove control timer
     */
    private Runnable mControlsRunnable;

    /**
     * The current Context
     */
    protected Context mContext;

    /**
     * The controls handler
     */
    private Handler mControlsHandler;

    /**
     * The elapsed time text view label
     */
    protected TextView mElapsedTimeText;

    /**
     * The time seek bar control
     */
    protected AppCompatSeekBar mSeekBar;

    /**
     * The Play Button
     */
    protected ImageView mPlayButton;

    /**
     * The Pause Button
     */
    protected ImageView mPauseButton;

    /**
     * The bottom views containers for adding and removing listeners
     */
    private List<View> mBottomViews;

    /**
     * The listener control
     */
    private View.OnClickListener mControlsListener;

    /**
     * Initialize controls
     *
     * @param clickListener The click listener to set into controls
     */
    @SuppressLint("NewApi")
    protected void initControls(View.OnClickListener clickListener,
                                SeekBar.OnSeekBarChangeListener seekBarChangeListener) {

        mControlsListener = clickListener;

        mBottomViews = new ArrayList<>();

        mControlsHandler = new Handler();

        mPlayButton = (ImageView) getPlayerControls()
                .findViewById(R.id.play_button);

        mPauseButton = (ImageView) getPlayerControls()
                .findViewById(R.id.pause_button);

        getPlayerControlsContainer().setOnClickListener(mControlsListener);
        getPlayerControls().setOnClickListener(mControlsListener);

        int buildVersion = Build.VERSION.SDK_INT;

        // Set thumb color for pre-lollipop devices to seek bar thumb
        if (buildVersion < Build.VERSION_CODES.LOLLIPOP) {

            int whiteColor = mContext.getResources().getColor(R.color.white);

            mSeekBar.getThumb()
                    .setColorFilter(whiteColor, PorterDuff.Mode.SRC_IN);
        }

        // Adding views into a list to handle listener easily
        mBottomViews.add(mPlayButton);
        mBottomViews.add(mPauseButton);

        // Adding listeners
        clearAddButtonListeners(true);

        mSeekBar.setOnSeekBarChangeListener(seekBarChangeListener);
    }

    /**
     * Clears or sets listener for bottom controls
     *
     * @param addListener If true then we will try to add it
     *                    if not then we will remove it
     */
    protected void clearAddButtonListeners(boolean addListener) {
        for (View view : mBottomViews) {
            if (addListener) {
                view.setOnClickListener(mControlsListener);
            } else {
                view.setOnClickListener(null);
            }
        }
    }

    /**
     * Show controls
     */
    protected void showControls() {
        if (getPlayerControls().getVisibility() == View.GONE) {
            Animation fadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);

            getPlayerControls().startAnimation(fadeIn);
        }

        getPlayerControls().setVisibility(View.VISIBLE);

        // Remove any pending runnable
        if (mControlsRunnable != null) {
            mControlsHandler.removeCallbacks(mControlsRunnable);
        }

        // Auto hide Controls runnable
        mControlsRunnable = new Runnable() {
            /**
             * On Runnable run
             */
            @Override
            public void run() {
                Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);

                fadeOut.setAnimationListener(new HideControlsAnimationListener());

                getPlayerControls().startAnimation(fadeOut);
            }
        };

        // Executing post delayed runnable to hide controls
        mControlsHandler.postDelayed(mControlsRunnable, CONTROLS_TIMEOUT);
    }

    /**
     * Reset controls state for lecture changes
     */
    protected void resetControlValues() {
        String timeLabel = StringUtil.timerDurationSetter(0);

        // Let´s reset the state
        mSeekBar.setProgress(0);

        mElapsedTimeText.setText(timeLabel);

        // Reset state play pause button state
        mPlayButton.setVisibility(View.GONE);
        mPauseButton.setVisibility(View.VISIBLE);
    }

    /**
     * No Network message helper
     *
     * @return No Network error message
     */
    protected String getNoNetworkMessage() {
        return mContext.getString(R.string.network_error_text);
    }

    /**
     * The Player controls view
     *
     * @return the player fragment controls view
     */
    protected RelativeLayout getPlayerControls() {
        return null; // ((TeamDetailFragment) mContext).getPlayerControls();
    }

    /**
     * The Player controls view
     *
     * @return the player fragment controls view
     */
    protected RelativeLayout getPlayerControlsContainer() {
        return null; // ((TeamDetailFragment) mContext).getPlayerControlsContainer();
    }

    /**
     * Helper for animation listener when the controls needs to disappear
     */
    private class HideControlsAnimationListener implements Animation.AnimationListener {
        /**
         * Executed when animation started
         *
         * @param animation The animation
         */
        @Override
        public void onAnimationStart(Animation animation) {
        }

        /**
         * Executed when animation ends
         *
         * @param animation The animation
         */
        @Override
        public void onAnimationEnd(Animation animation) {
            getPlayerControls().setVisibility(View.GONE);
        }

        /**
         * Executed when animation is set to be repeated
         *
         * @param animation The animation
         */
        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    }
}
