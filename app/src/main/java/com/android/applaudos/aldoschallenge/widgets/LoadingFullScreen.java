package com.android.applaudos.aldoschallenge.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.applaudos.aldoschallenge.R;
import com.android.applaudos.aldoschallenge.activities.BaseActivity;
import com.android.applaudos.aldoschallenge.utils.StringUtil;

/**
 * Loading Screen Helper Class
 *
 * @author Aldo Infanzon
 */
public class LoadingFullScreen {

    /**
     * The current context
     */
    private Context mContext;

    /**
     * The loading view
     */
    private View mLoadingView;

    /**
     * The retry container
     */
    private View mRetryContainer;

    /**
     * The Progress Container
     */
    private View mProgressContainer;

    /**
     * The Loading text
     */
    private RelativeLayout mRetryButton;

    /**
     * The Loading text
     */
    private TextView mLoadingText;

    /**
     * The loading Error Message
     */
    private TextView mLoadingErrorMessage;

    /**
     * The title for retry button
     */
    private TextView mButtonTitle;

    /**
     * Wherever the view is on or of
     */
    private boolean mIsVisible;

    /**
     * The loading screen
     */
    public LoadingFullScreen(Context context) {
        mContext = context;

        // Main View
        mLoadingView = ((BaseActivity) mContext).findViewById(R.id.view_loading);
        mLoadingView.setVisibility(View.GONE);

        // Setting up Loading Controls
        mLoadingText = (TextView) ((BaseActivity) mContext).findViewById(R.id.loading_text);
        mProgressContainer = ((BaseActivity) mContext)
                .findViewById(R.id.loading_progress_container);


        // Setting up error controls
        mRetryContainer = ((BaseActivity) mContext).findViewById(R.id.loading_retry_container);
        mLoadingErrorMessage = (TextView) ((BaseActivity) mContext)
                .findViewById(R.id.loading_error_text);
        mRetryButton = (RelativeLayout) ((BaseActivity) mContext)
                .findViewById(R.id.loading_retry_button);
        mButtonTitle = (TextView) ((BaseActivity) mContext)
                .findViewById(R.id.loading_retry_button_title);


        mIsVisible = false;
    }

    /**
     * Shows or hide the retry screen
     *
     * @param show              Wherever to show or hide. True for show
     * @param animationListener use this as null for none animation
     */
    public void showHideRetry(final boolean show,
                              final @Nullable Animation
                                      .AnimationListener animationListener) {

        if (mRetryContainer.getVisibility() == View.VISIBLE && show) {
            Log.w("Illegal _state", "The Retry view is already visible");

            return;
        }

        if (mProgressContainer.getVisibility() == View.GONE && !show) {
            Log.w("Illegal _state", "The loading container is already visible");

            return;
        }

        final BaseActivity baseActivity = ((BaseActivity) mContext);

        ((BaseActivity) mContext).runOnUiThread(new Runnable() {
            /**
             * On UI thread run
             */
            @Override
            public void run() {

                if (animationListener != null) {

                    if (show) {
                        baseActivity.getAnimationHelpers().fadeSwitchViews(
                                mProgressContainer,
                                mRetryContainer,
                                animationListener);
                    } else {
                        baseActivity.getAnimationHelpers().fadeSwitchViews(
                                mRetryContainer,
                                mProgressContainer,
                                animationListener);
                    }

                    return;
                }

                if (!show) {
                    mRetryContainer.setVisibility(View.GONE);
                    mProgressContainer.setVisibility(View.VISIBLE);
                    return;
                }

                mProgressContainer.setVisibility(View.GONE);
                mRetryContainer.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Shows or hide the loading screen
     *
     * @param show              Wherever to show or hide. True for show
     * @param animationListener use this as null for none animation
     */
    public void showHideLoading(final boolean show,
                                final @Nullable Animation
                                        .AnimationListener animationListener) {

        if (mIsVisible == show) {
            Log.w("Illegal _state", "The Main view is already " + show);

            return;
        }

        final BaseActivity baseActivity = ((BaseActivity) mContext);

        ((BaseActivity) mContext).runOnUiThread(new Runnable() {
            /**
             * On UI Thread run
             */
            @Override
            public void run() {

                if (animationListener != null) {

                    if (show) {
                        mIsVisible = true;

                        baseActivity.getAnimationHelpers().fadeInView(
                                mLoadingView,
                                animationListener);
                    } else {
                        mIsVisible = false;

                        baseActivity.getAnimationHelpers().fadeOutView(
                                mLoadingView,
                                animationListener);
                    }

                    return;
                }

                if (!show) {

                    mIsVisible = false;

                    ((BaseActivity) mContext).findViewById(R.id.view_loading)
                            .setVisibility(View.GONE);

                    return;
                }

                mIsVisible = true;

                mLoadingView.setVisibility(View.VISIBLE);
            }
        });
    }

    /**
     * Set loading messages
     *
     * @param message      The loading message
     * @param errorMessage The error message
     * @param buttonText   The error retry button title
     */
    public void setCustomValues(@Nullable String message,
                                @Nullable String errorMessage,
                                @Nullable String buttonText,
                                @Nullable View.OnClickListener retryListener) {

        if (!StringUtil.stringIsNullOrEmpty(message)) {
            mLoadingText.setText(message);
        }

        if (!StringUtil.stringIsNullOrEmpty(errorMessage)) {
            mLoadingErrorMessage.setText(errorMessage);
        }

        if (!StringUtil.stringIsNullOrEmpty(buttonText)) {
            mLoadingText.setText(buttonText);
        }

        mRetryButton.setOnClickListener(null);
        mRetryButton.setOnClickListener(retryListener);
    }
}
