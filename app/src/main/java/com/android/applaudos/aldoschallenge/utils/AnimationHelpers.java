package com.android.applaudos.aldoschallenge.utils;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

import com.android.applaudos.aldoschallenge.R;

/**
 * Helper class to use animation in views
 *
 * @author Aldo Infanzon on 5/11/2016.
 */
public class AnimationHelpers {

    /**
     * The Current Context
     */
    private Context mContext;

    /**
     * Class constructor
     *
     * @param context The current context
     */
    public AnimationHelpers(Context context) {
        mContext = context;
    }

    /**
     * Shrink a view with animation
     *
     * @param view              The view to be shrink
     * @param shrinkScale       the scale to use
     * @param duration          The duration
     * @param animationListener The animation listener for events
     * @return the animation
     */
    public Animation shrinkAnimation(final View view, float shrinkScale, long duration,
                                     @Nullable Animation.AnimationListener
                                             animationListener) {

        final ScaleAnimation scaleAnimation = new ScaleAnimation(
                shrinkScale, 1.0f,
                shrinkScale, 1.0f,
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 1f);

        scaleAnimation.setDuration(duration);
        scaleAnimation.setFillAfter(true);

        if (animationListener != null) {
            scaleAnimation.setAnimationListener(animationListener);
        }

        view.startAnimation(scaleAnimation);

        return scaleAnimation;
    }

    /**
     * Grows a view with animation
     *
     * @param view              The view to be shrink
     * @param shrinkScale       the scale to use
     * @param duration          The duration
     * @param animationListener The animation listener for events
     * @return the animation
     */
    public Animation growAnimation(final View view, float shrinkScale, long duration,
                                   @Nullable Animation.AnimationListener
                                           animationListener) {

        final ScaleAnimation scaleAnimation = new ScaleAnimation(
                1.0f, shrinkScale,
                1.0f, shrinkScale,
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 1f);

        scaleAnimation.setDuration(duration);
        scaleAnimation.setFillAfter(true);

        if (animationListener != null) {
            scaleAnimation.setAnimationListener(animationListener);
        }

        view.startAnimation(scaleAnimation);

        return scaleAnimation;
    }

    /**
     * Grows a view with animation
     *
     * @param shrinkView        The view to be shrink
     * @param scale             the scale to use
     * @param duration          The duration
     * @param animationListener The animation listener for events
     * @return the animation
     */
    public Animation shrinkGrowAnimation(final View shrinkView, final View growView,
                                         final float scale, final long duration,
                                         @Nullable final Animation.AnimationListener
                                                 animationListener) {

        Animation animation = shrinkAnimation(shrinkView, scale, duration,
                new Animation.AnimationListener() {
                    /**
                     * On animation start
                     * @param animation The animation
                     */
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    /**
                     * On Animation end. Will execute second animation
                     * @param animation The animation
                     */
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        growAnimation(growView, scale, duration, animationListener);
                    }

                    /**
                     * On animation repeat
                     * @param animation The animation
                     */
                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });

        return animation;
    }


    /**
     * Fade in View animation helper
     *
     * @param view              The view to be fade in
     * @param animationListener Nullable animation listener to listen to start end or animation repeat
     */
    public Animation fadeInView(final View view,
                                @Nullable Animation.AnimationListener animationListener) {

        Animation fadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        fadeIn.setRepeatCount(0);

        if (animationListener != null) {
            fadeIn.setAnimationListener(animationListener);
        }

        view.startAnimation(fadeIn);
        view.setVisibility(View.VISIBLE);

        Handler animationHandler = new Handler();

        animationHandler.postDelayed(new Runnable() {
            /**
             * Runnable when time passes
             */
            @Override
            public void run() {
                view.clearAnimation();
            }
        }, fadeIn.getDuration());

        return fadeIn;
    }

    /**
     * Fade out view animation helper
     *
     * @param view              The view to be fade in
     * @param animationListener Nullable animation listener to listen to start end or animation repeat
     */
    public Animation fadeOutView(final View view,
                                 @Nullable Animation.AnimationListener animationListener) {
        Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
        fadeOut.setRepeatCount(0);

        if (animationListener != null) {
            fadeOut.setAnimationListener(animationListener);
        }

        view.startAnimation(fadeOut);

        Handler animationHandler = new Handler();

        animationHandler.postDelayed(new Runnable() {
            /**
             * Runnable when time passes
             */
            @Override
            public void run() {
                view.clearAnimation();
                view.setVisibility(View.GONE);
            }
        }, fadeOut.getDuration());

        return fadeOut;
    }

    /**
     * Make simultaneous fade out and fade in two view
     *
     * @param fadeOutView The view that will be gone
     * @param fadeInView  The view that will be visible
     */
    public void fadeSwitchViews(View fadeOutView,
                                final View fadeInView,
                                @Nullable final Animation.AnimationListener animationListener) {

        fadeOutView(fadeOutView, null);

        Handler animationHandler = new Handler();

        animationHandler.postDelayed(new Runnable() {
            /**
             * Runnable when time passes
             */
            @Override
            public void run() {
                fadeInView(fadeInView, animationListener);
            }
        }, 300);
    }

    /**
     * Left in view animation helper
     *
     * @param view              The view to be fade in
     * @param animationListener Nullable animation listener to listen to start end or animation repeat
     */
    public Animation leftInView(final View view,
                                @Nullable Animation.AnimationListener animationListener) {

        view.clearAnimation();

        Animation leftIn = AnimationUtils.loadAnimation(mContext, R.anim.left_in);

        if (animationListener != null) {
            leftIn.setAnimationListener(animationListener);
        }

        view.startAnimation(leftIn);
        view.setVisibility(View.VISIBLE);
        return leftIn;
    }

    /**
     * Left out view animation helper
     *
     * @param view              The view to be fade in
     * @param animationListener Nullable animation listener to listen to start end or animation repeat
     */
    public Animation leftOutView(final View view,
                                 @Nullable Animation.AnimationListener animationListener) {

        view.clearAnimation();

        Animation leftOut = AnimationUtils.loadAnimation(mContext, R.anim.left_out);

        if (animationListener != null) {
            leftOut.setAnimationListener(animationListener);
        }

        view.startAnimation(leftOut);
        view.setVisibility(View.GONE);

        return leftOut;
    }

    /**
     * bottom in view animation helper
     *
     * @param view              The view to be fade in
     * @param animationListener Nullable animation listener to listen to start end or animation repeat
     */
    public Animation bottomInView(final View view,
                                  @Nullable Animation.AnimationListener animationListener) {

        view.clearAnimation();

        Animation bottomIn = AnimationUtils.loadAnimation(mContext, R.anim.bottom_in);

        if (animationListener != null) {
            bottomIn.setAnimationListener(animationListener);
        }

        view.startAnimation(bottomIn);
        view.setVisibility(View.VISIBLE);
        return bottomIn;
    }

    /**
     * bottom out view animation helper
     *
     * @param view              The view to be fade in
     * @param animationListener Nullable animation listener to listen to start end or animation repeat
     */
    public Animation bottomOutView(final View view,
                                   @Nullable Animation.AnimationListener animationListener) {

        view.clearAnimation();

        Animation bottomOut = AnimationUtils.loadAnimation(mContext, R.anim.bottom_out);

        if (animationListener != null) {
            bottomOut.setAnimationListener(animationListener);
        }

        view.startAnimation(bottomOut);

        return bottomOut;
    }

    /**
     * Expands a view with animation
     *
     * @param view The view to be expanded
     */
    public Animation expandView(final View view) {
        view.clearAnimation();

        view.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        final int targetHeight = view.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? ViewGroup.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        animation.setDuration((int) (targetHeight / view.getContext().getResources().getDisplayMetrics().density));
        view.startAnimation(animation);
        return animation;
    }

    /**
     * The view to be expanded
     *
     * @param view The view to collapse
     */
    public Animation collapseView(final View view) {
        view.clearAnimation();

        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        animation.setDuration((int) (initialHeight / view.getContext().getResources()
                .getDisplayMetrics().density));
        view.startAnimation(animation);
        return animation;
    }
}
