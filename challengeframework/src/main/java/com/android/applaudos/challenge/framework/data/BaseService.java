package com.android.applaudos.challenge.framework.data;

import com.android.applaudos.challenge.framework.constants.ServiceConstants;
import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Base Service Class
 *
 * @author Aldo Infanzon on 4/26/2016.
 */
public class BaseService {

    /**
     * The Service client
     */
    private OkHttpClient mServiceClient;

    /**
     * The base url to use
     */
    private String mBaseUrl;

    /**
     * Class constructor. Get Client from singleton instance
     *
     * @param client  The Service Client from a singleton instance
     * @param baseUrl The base url to use
     */
    public BaseService(OkHttpClient client, String baseUrl) {
        mServiceClient = client;
        mBaseUrl = baseUrl;
    }

    /**
     * Creates a new base simple get request
     *
     * @param url The url to set the request
     * @return The new request build
     */
    public Request simpleGetRequest(String url) {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        return request;
    }

    /**
     * Creates a new base request from base for post requests
     *
     * @param url        The url to set the request
     * @param postObject The object to transform in json to set in body
     * @return The new request build
     */
    public Request newBasePostCreateRequest(String url, Object postObject) {
        Gson gson = new Gson();

        String postBody = gson.toJson(postObject);

        RequestBody requestBody = RequestBody.create(
                ServiceConstants.MEDIA_TYPE_JSON, postBody);

        Request request = new Request.Builder()
                .url(url)
                .header(ServiceConstants.CONTENT_TYPE,
                        ServiceConstants.CONTENT_TYPE_JSON)
                .post(requestBody)
                .build();

        return request;
    }

    /**
     * The Service client
     *
     * @return The service client
     */
    public OkHttpClient getServiceClient() {
        return mServiceClient;
    }

    /**
     * The base url to use
     *
     * @return The base url
     */
    public String getBaseUrl() {
        return mBaseUrl;
    }
}