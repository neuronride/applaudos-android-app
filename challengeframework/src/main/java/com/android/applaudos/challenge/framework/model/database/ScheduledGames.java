package com.android.applaudos.challenge.framework.model.database;

import com.android.applaudos.challenge.framework.model.FrameworkDatabase;
import com.google.gson.annotations.Expose;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * The football team model definition class
 *
 * @author aldoinfanzon on 4/1/17
 */
@Table(database = FrameworkDatabase.class, name = ScheduledGames.TABLE_NAME)
public class ScheduledGames extends BaseModel {
    /**
     * The Table name
     */
    public final static String TABLE_NAME = "ScheduledGames";

    /**
     * The Team id
     */
    @Expose
    @PrimaryKey
    private int id;

    /**
     * The game date unfortunately is the only way to get some sort of key
     */
    @Expose
    @PrimaryKey
    private String date;

    /**
     * The game stadium
     */
    @Expose
    @Column
    private String stadium;

    /**
     * The Team id
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * The game date unfortunately is the only way to get some sort of key
     */
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    /**
     * The game stadium
     */
    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }
}
