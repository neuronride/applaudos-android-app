package com.android.applaudos.challenge.framework.model.database;

import com.android.applaudos.challenge.framework.model.FrameworkDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.lang.reflect.Type;
import java.util.List;

/**
 * The football team model definition class
 *
 * @author aldoinfanzon on 4/1/17
 */
@Table(database = FrameworkDatabase.class, name = Team.TABLE_NAME)
public class Team extends BaseModel {
    /**
     * The Table name
     */
    public final static String TABLE_NAME = "Team";

    /**
     * The Menu Item Id
     */
    @Expose
    @PrimaryKey
    private int id;

    /**
     * The Team Phone Number
     */
    @Expose
    @SerializedName("phone_number")
    @Column
    private String phoneNumber;

    /**
     * The Stadium Image
     */
    @Expose
    @SerializedName("img_stadium")
    @Column
    private String stadiumImage;

    /**
     * The Site for buying tickets
     */
    @Expose
    @SerializedName("tickets_url")
    @Column
    private String ticketsUrl;

    /**
     * The Stadium Name
     */
    @Expose
    @SerializedName("website")
    @Column
    private String teamUrl;

    /**
     * The team coach
     */
    @Expose
    @Column
    private String coach;

    /**
     * The Team full name
     */
    @Expose
    @SerializedName("team_name")
    @Column
    private String teamName;

    /**
     * Year where the team born
     */
    @Expose
    @SerializedName("since")
    @Column
    private String bornYear;

    /**
     * The Team Logo Image
     */
    @Expose
    @SerializedName("img_logo")
    @Column
    private String logoImage;

    /**
     * Year where the team born
     */
    @Expose
    @SerializedName("team_nickname")
    @Column
    private String nickname;

    /**
     * Video Url
     */
    @Expose
    @SerializedName("video_url")
    @Column
    private String videoUrl;

    /**
     * The Team address
     */
    @Expose
    @Column
    private String address;

    /**
     * Some team description
     */
    @Expose
    @Column
    private String description;

    /**
     * The team stadium name
     */
    @Expose
    @Column
    private String stadium;

    /**
     * The team stadium longitude
     */
    @Expose
    @Column
    private Double longitude;

    /**
     * The team stadium latitude
     */
    @Expose
    @Column
    private Double latitude;

    /**
     * The scheduled games for this team
     */
    @Expose
    @SerializedName("schedule_games")
    private List<ScheduledGames> scheduledGames;

    /**
     * Create an item from json string
     *
     * @param jsonString The json string
     * @return A Course Object
     */
    public static Team jsonToItem(String jsonString) {
        Team item;

        GsonBuilder builder = new GsonBuilder();

        Gson gson = builder
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        item = gson.fromJson(jsonString, Team.class);

        return item;
    }

    /**
     * Create an item from json string
     *
     * @param jsonString The json string
     * @return An item Object
     */
    public static List<Team> jsonToItemList(String jsonString) {
        List<Team> items;

        GsonBuilder builder = new GsonBuilder();

        Gson gson = builder
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        Type type = new TypeToken<List<Team>>() {
        }.getType();

        items = gson.fromJson(jsonString, type);


        return items;
    }

    /**
     * The Menu Item Id
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * The Team Phone Number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * The Stadium Image
     */
    public String getStadiumImage() {
        return stadiumImage;
    }

    public void setStadiumImage(String stadiumImage) {
        this.stadiumImage = stadiumImage;
    }

    /**
     * The Site for buying tickets
     */
    public String getTicketsUrl() {
        return ticketsUrl;
    }

    public void setTicketsUrl(String ticketsUrl) {
        this.ticketsUrl = ticketsUrl;
    }

    /**
     * The Stadium Name
     */
    public String getTeamUrl() {
        return teamUrl;
    }

    public void setTeamUrl(String teamUrl) {
        this.teamUrl = teamUrl;
    }

    /**
     * The team coach
     */
    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    /**
     * The Team full name
     */
    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    /**
     * Year where the team born
     */
    public String getBornYear() {
        return bornYear;
    }

    public void setBornYear(String bornYear) {
        this.bornYear = bornYear;
    }

    /**
     * The Team Logo Image
     */
    public String getLogoImage() {
        return logoImage;
    }

    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }

    /**
     * Year where the team born
     */
    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    /**
     * Video Url
     */
    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    /**
     * The Team address
     */
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * The team stadium name
     */
    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    /**
     * The scheduled games for this team
     */
    public List<ScheduledGames> getScheduledGames() {
        return scheduledGames;
    }

    public void setScheduledGames(List<ScheduledGames> scheduledGames) {
        this.scheduledGames = scheduledGames;
    }

    /**
     * Some team description
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * The team stadium longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * The team stadium latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
