package com.android.applaudos.challenge.framework.business.teams;

import android.content.Context;

import com.android.applaudos.challenge.framework.business.BaseBusiness;
import com.android.applaudos.challenge.framework.business.games.GamesBusiness;
import com.android.applaudos.challenge.framework.data.team.TeamData;
import com.android.applaudos.challenge.framework.data.team.TeamService;
import com.android.applaudos.challenge.framework.model.database.ScheduledGames;
import com.android.applaudos.challenge.framework.model.database.Team;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Team business layer class
 *
 * @author aldoinfanzon on 4/1/2017.
 */
public class TeamBusiness extends BaseBusiness {

    /**
     * The service manager
     */
    private TeamService mServiceManager;

    /**
     * The service manager
     */
    private TeamData mDataManager;

    /**
     * Class constructor. Get Client from singleton instance
     *
     * @param client  The Service Client from a singleton instance
     * @param context Use the application context
     */
    public TeamBusiness(Context context, OkHttpClient client) {
        super(context, client);

        mServiceManager = new TeamService(getServiceClient(), getBaseUrl());

        mDataManager = new TeamData();
    }

    /**
     * Login the user into the platform
     *
     * @param callback Callback after execution finishes
     * @return Call to cancel request at any time
     */
    public Call getTeams(Callback callback) {

        Call call = mServiceManager.getTeams(new OnTeamResponse(callback));

        return call;
    }

    /**
     * Login the user into the platform
     *
     * @param id The team id
     * @return Call to cancel request at any time
     */
    public Team getTeamDb(int id) {
        return mDataManager.getItem(id);
    }

    /**
     * Saves DB Menu Item List
     *
     * @param itemList   The item list
     * @param clearTable If the table should be cleared before storing
     * @return The item list
     */
    private List<Team> saveDbItemList(
            List<Team> itemList, boolean clearTable) {

        if (clearTable) {
            mDataManager.clearTable();
        }

        return mDataManager.addItemsRange(itemList);
    }

    /**
     * Get DB Menu list of items by async method
     *
     * @param callback The callback
     */
    public void getDbTeamsAsync(QueryTransaction.QueryResultCallback<Team> callback) {
        mDataManager.getItemsListAsync(callback);
    }

    /**
     * Get DB Menu list of items
     */
    public List<Team> getDbTeams() {
        return mDataManager.getItemsList();
    }

    /**
     * On Service Success Class holder
     */
    private class OnTeamResponse implements Callback {

        /**
         * The saved callback provider
         */
        private Callback mCallbackProvider;

        /**
         * On Service success saves callback provider
         *
         * @param callback The callback to be saved
         */
        OnTeamResponse(Callback callback) {
            mCallbackProvider = callback;
        }

        /**
         * When Service has an error
         *
         * @param call The call
         * @param e    The IO Exception
         */
        @Override
        public void onFailure(Call call, IOException e) {
            mCallbackProvider.onFailure(call, e);
        }

        /**
         * On Response
         *
         * @param call     The call
         * @param response The response
         * @throws IOException
         */
        @Override
        public void onResponse(Call call, Response response) throws IOException {

            if (response.code() == 200) {

                String jsonString = response.body().string();

                List<Team> teamResponseList = Team.jsonToItemList(jsonString);

                List<ScheduledGames> scheduledGames = new ArrayList<>();

                // Saving scheduled games into database in case we need them later
                for (Team team : teamResponseList) {
                    for (ScheduledGames game : team.getScheduledGames()) {
                        game.setId(team.getId());

                        scheduledGames.add(game);
                    }
                }

                saveDbItemList(teamResponseList, false);

                GamesBusiness gamesBusiness = new GamesBusiness();

                gamesBusiness.saveDbItemList(scheduledGames, false);

                // Note: method response.body().string() will clear body string making it null
            }

            // Calls original callback to respond to data fragment
            mCallbackProvider.onResponse(call, response);
        }
    }
}
