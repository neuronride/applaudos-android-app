package com.android.applaudos.challenge.framework.business.games;

import com.android.applaudos.challenge.framework.business.BaseBusiness;
import com.android.applaudos.challenge.framework.data.games.ScheduledGameData;
import com.android.applaudos.challenge.framework.model.database.ScheduledGames;

import java.util.List;

/**
 * Team business layer class
 *
 * @author aldoinfanzon on 4/1/2017.
 */
public class GamesBusiness extends BaseBusiness {

    /**
     * The data manager
     */
    private ScheduledGameData mDataManager;

    /**
     * Class constructor. Get Client from singleton instance
     */
    public GamesBusiness() {
        super();

        mDataManager = new ScheduledGameData();
    }

    /**
     * The last game to share
     * @param id The game id
     * @return The game
     */
    public ScheduledGames getLastGame(int id) {
        return mDataManager.getItem(id);
    }

    /**
     * Saves DB Menu Item List
     *
     * @param itemList   The item list
     * @param clearTable If the table should be cleared before storing
     * @return The item list
     */
    public List<ScheduledGames> saveDbItemList(
            List<ScheduledGames> itemList, boolean clearTable) {

        if (clearTable) {
            mDataManager.clearTable();
        }

        return mDataManager.addItemsRange(itemList);
    }
}
