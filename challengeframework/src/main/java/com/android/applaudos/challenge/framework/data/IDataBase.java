package com.android.applaudos.challenge.framework.data;

import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.util.List;

/**
 * @author Aldo Infanzon on 4/20/2016.
 */
public interface IDataBase<T> {

    /**
     * Creates a new record
     *
     * @param item The new item to be created
     * @return The item
     */
    T createItem(T item);

    /**
     * Whenever we need to add a range of items into database
     *
     * @param itemsList
     * @return The added list of items
     */
    List<T> addItemsRange(List<T> itemsList);

    /**
     * Updates a current item
     *
     * @param item The item
     * @return The updated item
     */
    T updateItem(T item);

    /**
     * Get certain item
     *
     * @param id The item id
     * @return The item
     */
    T getItem(int id);

    /**
     * Gets a list of items by certain properties
     *
     * @param callback The Transaction listener to support callback operation
     * @return The selected list
     */
    void getItemsListAsync(QueryTransaction.QueryResultCallback<T> callback);

    /**
     * Gets a list of items by certain properties
     *
     * @return The selected list
     */
    List<T> getItemsList();

    /**
     * Deletes certain item
     *
     * @param id The item id to be deleted
     * @return True for success
     */
    void deleteItem(int id);

    /**
     * Clears the whole table
     */
    void clearTable();
}
