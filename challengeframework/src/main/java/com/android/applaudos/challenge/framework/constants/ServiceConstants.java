package com.android.applaudos.challenge.framework.constants;

import okhttp3.MediaType;

/**
 * Service Constants as headers or other setups
 *
 * @author Aldo Infanzon on 4/26/2016.
 */
public class ServiceConstants {

    /**
     * The default timeout
     */
    public static final int DEFAULT_TIMEOUT = 5000;

    /**
     * The default timeout
     */
    public static final int DEFAULT_WRITE_TIMEOUT = 5000;

    /**
     * The default timeout
     */
    public static final int DEFAULT_READ_TIMEOUT = 5000;

    /**
     * Content Type for Json
     */
    public static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";

    /**
     * Content Type Header
     */
    public static final String CONTENT_TYPE = "Content-Type";

    /**
     * Accept Json Header Value
     */
    public static final String HEADER_ACCEPT_JSON = "application/json";

    /**
     * Media Type for Json
     */
    public static final MediaType MEDIA_TYPE_JSON
            = MediaType.parse("application/json; charset=utf-8");

    /**
     * Package for Production Environments
     */
    public static final String SERVICE_BASE_URL = "http://applaudostudios.com/external/";

    /**
     * For get football teams
     */
    public static final String METHOD_GET_TEAM_HOMEWORK_JSON = "applaudo_homework.json";

    /**
     * The google play store location
     */
    public static final String GOOGLE_APP_STORE_LOCATION =
            "https://play.google.com/store/apps/details?id={package_id}";

    /**
     * The google play store location
     */
    public static final String AMAZON_APP_STORE_LOCATION =
            "https://www.amazon.com/The-Teaching-Company-Great-Courses/dp/B00CYNPAEC";

    /**
     * Market app link
     */
    public static final String GOOGLE_MARKET_APP_LINK =
            "market://details?id={package_id}";

    /**
     * Market app link for amazon
     */
    public static final String AMAZON_MARKET_APP_LINK =
            "amzn://apps/android?p={package_id}";
}
