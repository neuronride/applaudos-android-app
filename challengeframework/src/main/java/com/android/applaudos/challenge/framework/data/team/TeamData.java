package com.android.applaudos.challenge.framework.data.team;

import com.android.applaudos.challenge.framework.data.IDataBase;
import com.android.applaudos.challenge.framework.model.FrameworkDatabase;
import com.android.applaudos.challenge.framework.model.database.Team;
import com.android.applaudos.challenge.framework.model.database.Team_Table;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.util.List;

/**
 * The team data layer class
 *
 * @author Aldo Infanzon on 11/22/2016.
 */
public class TeamData implements IDataBase<Team> {
    @Override
    public Team createItem(Team item) {
        return null;
    }

    /**
     * Saves a range of menu items
     *
     * @param itemsList The items list
     * @return The saved list
     */
    @Override
    public List<Team> addItemsRange(final List<Team> itemsList) {
        FlowManager.getDatabase(FrameworkDatabase.class).executeTransaction(new ITransaction() {
            /**
             * Executes items range
             *
             * @param databaseWrapper The database range
             */
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                // something here
                for (Team item : itemsList) {
                    item.save(databaseWrapper);
                }
            }
        });

        return itemsList;
    }

    /**
     * Updates certain item
     *
     * @param item The item
     * @return the item updated
     */
    @Override
    public Team updateItem(Team item) {
        throw new IllegalStateException();
    }

    /**
     * Get Certain item by it's id
     *
     * @param id The item id
     * @return The item by it's id
     */
    @Override
    public Team getItem(int id) {

        Team item = SQLite.select()
                .from(Team.class)
                .where(Team_Table.id.eq(id))
                .querySingle();

        return item;
    }

    /**
     * Retrieve all table items using async operation
     *
     * @param callback The Transaction listener to support callback operation
     */
    @Override
    public void getItemsListAsync(QueryTransaction.QueryResultCallback<Team> callback) {
        SQLite.select()
                .from(Team.class)
                .orderBy(Team_Table.teamName, true)
                .async()
                .queryResultCallback(callback).execute();
    }

    /**
     * Get all item list order by id
     *
     * @return The list of items
     */
    @Override
    public List<Team> getItemsList() {
        return SQLite.select()
                .from(Team.class)
                .orderBy(Team_Table.teamName, true)
                .queryList();
    }

    @Override
    public void deleteItem(int id) {
        throw new IllegalStateException();
    }

    /**
     * Clears the table for new data
     */
    @Override
    public void clearTable() {
        SQLite.delete(Team.class)
                .execute();
    }
}
