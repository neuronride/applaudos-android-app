package com.android.applaudos.challenge.framework.business;

import android.content.Context;

import com.android.applaudos.challenge.framework.constants.BaseConstants;
import com.android.applaudos.challenge.framework.constants.ServiceConstants;

import okhttp3.OkHttpClient;

/**
 * Base Business Class
 *
 * @author Aldo Infanzon on 4/26/2016.
 */
public class BaseBusiness {

    /**
     * The Service client
     */
    private OkHttpClient mServiceClient;

    /**
     * The base url to use
     */
    private String mBaseUrl;

    /**
     * Class constructor. Get Client from singleton instance
     *
     * @param client  The Service Client from a singleton instance
     * @param context Use the application context
     */
    public BaseBusiness(Context context, OkHttpClient client) {

        mServiceClient = client;

        String packageName = context.getPackageName();

        // Setting up base url base on package name
        switch (packageName) {
            case BaseConstants.APP_PACKAGE_DEBUG: // For app in stage case

                mBaseUrl = ServiceConstants.SERVICE_BASE_URL;

                break;
            case BaseConstants.APP_PACKAGE_PRODUCTION:
            default:

                mBaseUrl = ServiceConstants.SERVICE_BASE_URL;

                break;
        }
    }

    /**
     * Class Constructor initialize ok http  from scratch and  uses stage url
     */
    public BaseBusiness() {
        mServiceClient = new OkHttpClient();

        mBaseUrl = ServiceConstants.SERVICE_BASE_URL;
    }

    /**
     * The Service client
     */
    public OkHttpClient getServiceClient() {
        return mServiceClient;
    }

    /**
     * The base url to use
     *
     * @return The base url
     */
    protected String getBaseUrl() {
        return mBaseUrl;
    }

    /**
     * Set the base url
     *
     * @param baseUrl the base url
     */
    protected void setBaseUrl(String baseUrl) {
        mBaseUrl = baseUrl;
    }
}
