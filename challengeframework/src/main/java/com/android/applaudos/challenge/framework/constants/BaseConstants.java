package com.android.applaudos.challenge.framework.constants;

/**
 * App Base Constants
 *
 * @author Aldo Infanzon on 4/26/2016.
 */
public class BaseConstants {
    /**
     * Package for Stage Environments
     */
    public static final String APP_PACKAGE_DEBUG = "com.android.applaudos.debug.aldoschallenge";

    /**
     * Package for Production Environments
     */
    public static final String APP_PACKAGE_PRODUCTION = "com.android.applaudos.aldoschallenge";

    /**
     * Info Encryption Seed
     */
    public static final String ENCRYPT_SEED = "367490";
}
