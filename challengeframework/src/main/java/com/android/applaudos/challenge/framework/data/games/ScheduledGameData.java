package com.android.applaudos.challenge.framework.data.games;

import com.android.applaudos.challenge.framework.data.IDataBase;
import com.android.applaudos.challenge.framework.model.FrameworkDatabase;
import com.android.applaudos.challenge.framework.model.database.ScheduledGames;
import com.android.applaudos.challenge.framework.model.database.ScheduledGames_Table;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.QueryTransaction;

import java.util.List;

/**
 * The ScheduledGames data layer class
 *
 * @author  on 11/22/2016.
 */
public class ScheduledGameData implements IDataBase<ScheduledGames> {
    @Override
    public ScheduledGames createItem(ScheduledGames item) {
        return null;
    }

    /**
     * Saves a range of menu items
     *
     * @param itemsList The items list
     * @return The saved list
     */
    @Override
    public List<ScheduledGames> addItemsRange(final List<ScheduledGames> itemsList) {
        FlowManager.getDatabase(FrameworkDatabase.class).executeTransaction(new ITransaction() {
            /**
             * Executes items range
             *
             * @param databaseWrapper The database range
             */
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                // something here
                for (ScheduledGames item : itemsList) {
                    item.save(databaseWrapper);
                }
            }
        });

        return itemsList;
    }

    /**
     * Updates certain item
     *
     * @param item The item
     * @return the item updated
     */
    @Override
    public ScheduledGames updateItem(ScheduledGames item) {
        throw new IllegalStateException();
    }

    /**
     * Get Certain item by it's id
     *
     * @param id The item id
     * @return The item by it's id
     */
    @Override
    public ScheduledGames getItem(int id) {
        ScheduledGames item = SQLite.select()
                .from(ScheduledGames.class)
                .where(ScheduledGames_Table.id.eq(id))
                .orderBy(ScheduledGames_Table.date, false)
                .querySingle();

        return item;
    }


    /**
     * Retrieve all table items using async operation
     *
     * @param callback The Transaction listener to support callback operation
     */
    @Override
    public void getItemsListAsync(QueryTransaction.QueryResultCallback<ScheduledGames> callback) {
        SQLite.select()
                .from(ScheduledGames.class)
                .orderBy(ScheduledGames_Table.id, true)
                .async()
                .queryResultCallback(callback).execute();
    }

    /**
     * Retrieve all table items
     * @return The table items
     */
    @Override
    public List<ScheduledGames> getItemsList() {
        return SQLite.select()
                .from(ScheduledGames.class)
                .orderBy(ScheduledGames_Table.id, true)
                .queryList();
    }

    @Override
    public void deleteItem(int id) {
        throw new IllegalStateException();
    }

    /**
     * Clears the table for new data
     */
    @Override
    public void clearTable() {
        SQLite.delete(ScheduledGames.class)
                .execute();
    }
}
