package com.android.applaudos.challenge.framework.model;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * The database main file for current app Database
 *
 * @author Aldo Infanzon on 11/22/2016.
 */
@Database(name = FrameworkDatabase.NAME, version = FrameworkDatabase.VERSION)
public class FrameworkDatabase {

    /**
     * Guero Alteno Database
     */
    public static final String NAME = "ChallengeFrameworkDatabase";

    /**
     * The Version of the db
     */
    public static final int VERSION = 1;
}
