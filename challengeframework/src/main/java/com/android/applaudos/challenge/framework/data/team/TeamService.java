package com.android.applaudos.challenge.framework.data.team;

import com.android.applaudos.challenge.framework.constants.ServiceConstants;
import com.android.applaudos.challenge.framework.data.BaseService;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Service helper to request Teams
 * @author aldoinfanzon on 4/1/2017.
 */
public class TeamService extends BaseService {
    /**
     * Class constructor. Get Client from singleton instance
     *
     * @param client  The Service Client from a singleton instance
     * @param baseUrl The base url to use
     */
    public TeamService(OkHttpClient client, String baseUrl) {
        super(client, baseUrl);
    }

    /**
     * Get all the teams from the internet
     *
     * @param callback The Callback after the method finishes
     * @return Call to cancel request at any time
     */
    public Call getTeams(Callback callback) {

        String url = getBaseUrl() + ServiceConstants.METHOD_GET_TEAM_HOMEWORK_JSON;

        Request request = simpleGetRequest(url);

        Call call = getServiceClient().newCall(request);

        call.enqueue(callback);

        return call;
    }
}
